# ubi-missing-packages

* This script provides an insight into packages that are not available in UBI (Universal Base Image) when compared to RHEL (RedHat Enterprise Linux)
* Hol' up... What exactly is UBI? Please see [What is Red Hat Universal Base Image?](https://developers.redhat.com/blog/2019/10/09/what-is-red-hat-universal-base-image) for more details about what UBI is.
* UBI packages have a different [EULA](https://www.redhat.com/en/about/agreements#UBI) to RHEL, and thus can be freely distributed. The CDN for UBI packages is publicaly available to all at [https://cdn-ubi.redhat.com/content/public/ubi](https://cdn-ubi.redhat.com/content/public/ubi)

* Script functionality

```
$ ./ubi-missing-packages.sh 
usage: ./ubi-missing-packages.sh -f <rhel7,rhel8> -m

This script provides an insight into packages that are not available in UBI when compared to RHEL

OPTIONS:

  -f        family, rhel7 or rhel8 (or both rhel7,rhel8)
  -m        create a markdown report (stored in ./output/)
  -v        run with extra verbosity
  -h        display this help
```

* Example execution:

```
$ ./ubi-missing-packages.sh -f rhel7,rhel8 -m 
Markdown of missing packages created in ./output/20220118-missing-ubi7-packages.md
Markdown of missing packages created in ./output/20220118-missing-ubi8-packages.md
```

* Example markdown:

[output](output)
