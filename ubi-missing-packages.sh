#!/bin/bash

TMPDIRECTORY=`mktemp -d`

RHEL7_REPOS="os optional extras rhscl"
RHEL8_REPOS="baseos appstream codeready-builder"
ARCHLIST="x86_64,noarch"
VERBOSE=0
MARKDOWN=0

usage() {
cat << EOF
usage: $0 -f <rhel7,rhel8> -m

This script provides an insight into packages that are not available in UBI when compared to RHEL

OPTIONS:

  -f        family, rhel7 or rhel8 (or both rhel7,rhel8)
  -m        create a markdown report (stored in ./output/)
  -v        run with extra verbosity
  -h        display this help
EOF
exit
}

while getopts "f:mvh" option
do
  case $option in
    h)
      usage
      ;;
    m)
      MARKDOWN=1
      mkdir output 2>/dev/null 
      ;;
    v)
      VERBOSE=1
      ;;
    f)
      FAMILY=`echo $OPTARG | sed 's/,/ /g'`
      ;;
  esac
done

if [ -z "$FAMILY" ]; then
  usage
fi

format_repo () {
  if [[ $1 == "os" ]]; then
    FORMATTED_REPO="os"
  elif [[ $1 == "rhscl" ]]; then
    FORMATTED_REPO="rhscl/1/os"
  else
    FORMATTED_REPO="$1/os"
  fi
}

process_repo () {
  format_repo $REPO
  [ $VERBOSE -eq 1 ] && echo "Working on ubi${DIST}.${REPO} -> $TMPDIRECTORY/ubi${DIST}.${REPO}"
  repoquery --repofrompath=randomid,${UBI_PREFIX}/${FORMATTED_REPO} --disablerepo=* --enablerepo=randomid -qa --qf="%{name}:%{version}:%{release}:%{arch}" --archlist=$ARCHLIST > $TMPDIRECTORY/ubi${DIST}.${REPO}
  [ $VERBOSE -eq 1 ] && echo "Working on rhel${DIST}.${REPO} -> $TMPDIRECTORY/rhel${DIST}.${REPO}"
  repoquery --repofrompath=randomid,${RHEL_PREFIX}/${FORMATTED_REPO} --disablerepo=* --enablerepo=randomid -qa --qf="%{name}:%{version}:%{release}:%{arch}" --archlist=$ARCHLIST > $TMPDIRECTORY/rhel${DIST}.${REPO}
  [ $VERBOSE -eq 1 ] && echo "ubi${DIST} (${REPO}) missing packages -> $TMPDIRECTORY/ubi${DIST}.${REPO}.missing"
  diff $TMPDIRECTORY/rhel${DIST}.${REPO} $TMPDIRECTORY/ubi${DIST}.${REPO} |grep \< | awk '{print $2}' | cut -d: -f1 | sort -u > $TMPDIRECTORY/ubi${DIST}.${REPO}.missing
}

if [[ $FAMILY == *"rhel7"* ]]; then
  DIST=7
  RHEL_PREFIX="https://linuxsoft.cern.ch/cdn.redhat.com/content/dist/rhel/server/7/7Server/x86_64"
  UBI_PREFIX="https://cdn-ubi.redhat.com/content/public/ubi/dist/ubi/server/7/7Server/x86_64"
  for REPO in $RHEL7_REPOS; do
    process_repo $REPO
  done
  if [ $MARKDOWN ]; then
    OUTPUT_FILE="./output/`date +%Y%m%d`-missing-ubi${DIST}-packages.md"
    echo -e "# Missing packages from the UBI (Universal Base Image) compared to RHEL (for the ${DIST} family)\n" > $OUTPUT_FILE
    echo -e "* Report generated on `date`\n" >> $OUTPUT_FILE
    echo -e "| Missing in UBI os | Missing in UBI extras | Missing in UBI optional | Missing in UBI rhscl |\n| ------ | ------ | ------ | ------ |" >> $OUTPUT_FILE
    paste $TMPDIRECTORY/ubi7.os.missing $TMPDIRECTORY/ubi7.extras.missing $TMPDIRECTORY/ubi7.optional.missing $TMPDIRECTORY/ubi7.rhscl.missing | while read os extras optional rhscl; do
      echo "| $os | $extras | $optional | $rhscl |" >> $OUTPUT_FILE
    done
    echo "Markdown of missing packages created in $OUTPUT_FILE"
  fi
fi

if [[ $FAMILY == *"rhel8"* ]]; then
  DIST=8
  RHEL_PREFIX="https://linuxsoft.cern.ch/cdn.redhat.com/content/dist/rhel8/8/x86_64"
  UBI_PREFIX="https://cdn-ubi.redhat.com/content/public/ubi/dist/ubi8/8/x86_64"
  for REPO in $RHEL8_REPOS; do
    process_repo $REPO
  done
  if [ $MARKDOWN ]; then
    OUTPUT_FILE="./output/`date +%Y%m%d`-missing-ubi${DIST}-packages.md"
    echo -e "# Missing packages from the UBI (Universal Base Image) compared to RHEL (for the ${DIST} family)\n" > $OUTPUT_FILE
    echo -e "* Report generated on `date`\n" >> $OUTPUT_FILE
    echo -e "| Missing in UBI BaseOS | Missing in UBI AppStream | Missing in UBI Codeready-builder |\n| ------ | ------ | ------ |" >> $OUTPUT_FILE
    paste $TMPDIRECTORY/ubi8.baseos.missing $TMPDIRECTORY/ubi8.appstream.missing $TMPDIRECTORY/ubi8.codeready-builder.missing | while read baseos appstream codeready; do
      echo "| $baseos | $appstream | $codeready |" >> $OUTPUT_FILE
    done
    echo "Markdown of missing packages created in $OUTPUT_FILE"
  fi
fi
