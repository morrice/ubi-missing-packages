# Missing packages from the UBI (Universal Base Image) compared to RHEL (for the 8 family)

* Report generated on Tue Jan 18 13:56:28 CET 2022

| Missing in UBI BaseOS | Missing in UBI AppStream | Missing in UBI Codeready-builder |
| ------ | ------ | ------ |
| aajohan-comfortaa-fonts | 389-ds-base | accel-config-devel |
| accel-config | 389-ds-base-devel | accountsservice-devel |
| accel-config-libs | 389-ds-base-legacy-tools | ant |
| acpica-tools | 389-ds-base-libs | ant-antlr |
| adcli | 389-ds-base-snmp | ant-apache-bcel |
| adcli-doc | abrt | ant-apache-bsf |
| alsa-sof-firmware | abrt-addon-ccpp | ant-apache-log4j |
| alsa-sof-firmware-debug | abrt-addon-coredump-helper | ant-apache-oro |
| arpwatch | abrt-addon-kerneloops | ant-apache-regexp |
| atlas-corei2 | abrt-addon-pstoreoops | ant-apache-resolver |
| atlas-corei2-devel | abrt-addon-vmcore | ant-apache-xalan2 |
| attr | abrt-addon-xorg | ant-commons-logging |
| audispd-plugins | abrt-cli | ant-commons-net |
| audispd-plugins-zos | abrt-cli-ng | ant-contrib |
| audit | abrt-console-notification | ant-contrib-javadoc |
| audit-libs-devel | abrt-dbus | ant-javadoc |
| augeas | abrt-desktop | ant-javamail |
| authselect | abrt-gui | ant-jdepend |
| authselect-libs | abrt-gui-libs | ant-jmf |
| autofs | abrt-java-connector | ant-jsch |
| avahi | abrt-libs | ant-junit |
| avahi-autoipd | abrt-plugin-machine-id | ant-lib |
| avahi-gobject | abrt-plugin-sosreport | antlr-C++ |
| bash-doc | abrt-tui | antlr-javadoc |
| bind-export-devel | accountsservice | antlr-manual |
| bind-export-libs | accountsservice-libs | antlr-tool |
| biosdevname | acpid | ant-manual |
| blktrace | adwaita-gtk2-theme | ant-swing |
| bluez | adwaita-qt | ant-testutil |
| bluez-hid2hci | adwaita-qt5 | ant-xz |
| bluez-libs | aide | aopalliance |
| bluez-obexd | alsa-firmware | aopalliance-javadoc |
| bolt | alsa-lib-devel | apache-commons-beanutils |
| boom-boot | alsa-plugins-arcamav | apache-commons-beanutils-javadoc |
| boom-boot-conf | alsa-plugins-maemo | apache-commons-cli |
| boom-boot-grub2 | alsa-plugins-oss | apache-commons-cli-javadoc |
| bpftool | alsa-plugins-pulseaudio | apache-commons-codec |
| bubblewrap | alsa-plugins-samplerate | apache-commons-codec-javadoc |
| cachefilesd | alsa-plugins-speex | apache-commons-collections |
| c-ares | alsa-plugins-upmix | apache-commons-collections-javadoc |
| c-ares-devel | alsa-plugins-usbstream | apache-commons-collections-testframework |
| chrony | alsa-plugins-vdownmix | apache-commons-compress |
| chrpath | alsa-tools-firmware | apache-commons-compress-javadoc |
| cifs-utils | alsa-ucm | apache-commons-exec |
| cockpit | alsa-utils | apache-commons-exec-javadoc |
| cockpit-bridge | alsa-utils-alsabat | apache-commons-io |
| cockpit-doc | amanda | apache-commons-io-javadoc |
| cockpit-system | amanda-client | apache-commons-jxpath |
| cockpit-ws | amanda-libs | apache-commons-jxpath-javadoc |
| compat-hwloc1 | amanda-server | apache-commons-lang |
| conntrack-tools | anaconda | apache-commons-lang3 |
| crda | anaconda-core | apache-commons-lang3-javadoc |
| cronie-noanacron | anaconda-dracut | apache-commons-lang-javadoc |
| cryptsetup | anaconda-gui | apache-commons-logging |
| cryptsetup-reencrypt | anaconda-install-env-deps | apache-commons-logging-javadoc |
| ctdb | anaconda-tui | apache-commons-net |
| ctdb-tests | anaconda-user-help | apache-commons-net-javadoc |
| cyrus-sasl-gs2 | anaconda-widgets | apache-commons-parent |
| cyrus-sasl-ldap | annobin-annocheck | apache-ivy |
| cyrus-sasl-md5 | ansible-collection-microsoft-sql | apache-ivy-javadoc |
| cyrus-sasl-ntlm | ansible-collection-redhat-rhel_mgmt | apache-parent |
| cyrus-sasl-scram | ansible-freeipa | apache-resource-bundles |
| daxctl | ansible-freeipa-tests | aqute-bnd |
| daxctl-libs | ansible-pcp | aqute-bnd-javadoc |
| dbxtool | ant | aqute-bndlib |
| dejavu-sans-mono-fonts | ant-lib | asciidoc-doc |
| dejavu-serif-fonts | apache-commons-collections | asio-devel |
| device-mapper-multipath | apache-commons-collections-javadoc | aspell-devel |
| device-mapper-multipath-libs | apache-commons-collections-testframework | assertj-core |
| dhcp-client | apache-commons-compress | assertj-core-javadoc |
| dhcp-common | apache-commons-jxpath | atinject |
| dhcp-libs | apache-commons-lang | atinject-javadoc |
| dhcp-relay | apache-commons-lang-javadoc | atinject-tck |
| dhcp-server | apache-commons-net | atkmm-devel |
| dlm-lib | apiguardian | atkmm-doc |
| dnf-automatic | appstream-data | augeas-devel |
| dnf-utils | apr-util-mysql | autoconf213 |
| dosfstools | apr-util-odbc | autogen |
| dracut-caps | apr-util-pgsql | autogen-libopts-devel |
| dracut-config-generic | apr-util-sqlite | autotrace |
| dracut-config-rescue | asciidoc | avahi-compat-howl |
| dracut-live | aspell | avahi-compat-howl-devel |
| dracut-network | aspell-en | avahi-compat-libdns_sd |
| dracut-squash | atk-devel | avahi-compat-libdns_sd-devel |
| dracut-tools | atkmm | avahi-devel |
| dump | at-spi2-atk-devel | avahi-glib-devel |
| e2fsprogs | at-spi2-core-devel | avahi-gobject-devel |
| e2fsprogs-devel | authd | avahi-ui |
| e2fsprogs-libs | authselect-compat | avahi-ui-devel |
| efibootmgr | autocorr-af | babl-devel |
| efi-filesystem | autocorr-bg | babl-devel-docs |
| efivar | autocorr-ca | bcc-devel |
| efivar-libs | autocorr-cs | bcc-doc |
| elfutils-debuginfod | autocorr-da | bcel |
| elfutils-debuginfod-client | autocorr-de | bcel-javadoc |
| elfutils-debuginfod-client-devel | autocorr-en | beust-jcommander |
| elfutils-devel | autocorr-es | beust-jcommander-javadoc |
| elfutils-libelf-devel | autocorr-fa | bison-devel |
| expect | autocorr-fi | blas-devel |
| fcoe-utils | autocorr-fr | bluez-libs-devel |
| firewalld | autocorr-ga | bnd-maven-plugin |
| firewalld-filesystem | autocorr-hr | boost-build |
| freeipmi | autocorr-hu | boost-doc |
| freeipmi-bmc-watchdog | autocorr-is | boost-examples |
| freeipmi-ipmidetectd | autocorr-it | boost-graph-mpich |
| freeipmi-ipmiseld | autocorr-ja | boost-graph-openmpi |
| fuse-devel | autocorr-ko | boost-jam |
| fwupd | autocorr-lb | boost-mpich |
| fwupdate | autocorr-lt | boost-mpich-devel |
| fwupdate-efi | autocorr-mn | boost-mpich-python3 |
| fwupdate-libs | autocorr-nl | boost-numpy3 |
| fxload | autocorr-pl | boost-openmpi |
| gamin | autocorr-pt | boost-openmpi-devel |
| gdbm-devel | autocorr-ro | boost-openmpi-python3 |
| gdisk | autocorr-ru | boost-python3 |
| genwqe-tools | autocorr-sk | boost-python3-devel |
| genwqe-vpd | autocorr-sl | boost-static |
| genwqe-zlib | autocorr-sr | brasero-devel |
| genwqe-zlib-devel | autocorr-sv | brlapi-devel |
| gettext-common-devel | autocorr-tr | brotli-devel |
| gettext-devel | autocorr-vi | bsf |
| gfs2-utils | autocorr-zh | bsf-javadoc |
| glib2-fam | avahi-ui-gtk3 | bsh |
| glib2-tests | babl | bsh-javadoc |
| glibc-langpack-aa | bacula-client | bsh-manual |
| glibc-langpack-af | bacula-common | byaccj |
| glibc-langpack-agr | bacula-console | cairomm-devel |
| glibc-langpack-ak | bacula-director | cairomm-doc |
| glibc-langpack-am | bacula-libs | cal10n |
| glibc-langpack-an | bacula-libs-sql | cal10n-javadoc |
| glibc-langpack-anp | bacula-logwatch | cdi-api |
| glibc-langpack-ar | bacula-storage | cdi-api-javadoc |
| glibc-langpack-as | baobab | cdparanoia-devel |
| glibc-langpack-ast | batik-css | celt051-devel |
| glibc-langpack-ayc | batik-util | cglib |
| glibc-langpack-az | bcc | cglib-javadoc |
| glibc-langpack-be | bcc-tools | clucene-core-devel |
| glibc-langpack-bem | bea-stax | clutter-devel |
| glibc-langpack-ber | bea-stax-api | clutter-doc |
| glibc-langpack-bg | bea-stax-javadoc | clutter-gst3-devel |
| glibc-langpack-bhb | beignet | clutter-gtk-devel |
| glibc-langpack-bho | bind | codemodel |
| glibc-langpack-bi | bind-chroot | cogl-devel |
| glibc-langpack-bn | bind-devel | cogl-doc |
| glibc-langpack-bo | bind-dyndb-ldap | colord-devel |
| glibc-langpack-br | bind-lite-devel | colord-devel-docs |
| glibc-langpack-brx | bind-pkcs11 | colord-gtk-devel |
| glibc-langpack-bs | bind-pkcs11-devel | compat-guile18 |
| glibc-langpack-byn | bind-pkcs11-libs | compat-guile18-devel |
| glibc-langpack-ca | bind-pkcs11-utils | corosync-vqsim |
| glibc-langpack-ce | bind-sdb | cppcheck |
| glibc-langpack-chr | bind-sdb-chroot | cppunit |
| glibc-langpack-cmn | binutils-devel | cppunit-devel |
| glibc-langpack-crh | bison | cppunit-doc |
| glibc-langpack-cs | bison-runtime | cracklib-devel |
| glibc-langpack-csb | bitmap-console-fonts | crash-devel |
| glibc-langpack-cv | bitmap-fangsongti-fonts | ctags-etags |
| glibc-langpack-cy | bitmap-fixed-fonts | CUnit-devel |
| glibc-langpack-da | bitmap-fonts-compat | cups-filters-devel |
| glibc-langpack-de | bitmap-lucida-typewriter-fonts | daxctl-devel |
| glibc-langpack-doi | blas | dblatex |
| glibc-langpack-dsb | blas64 | dbus-c++ |
| glibc-langpack-dv | blivet-data | dbus-c++-devel |
| glibc-langpack-dz | bluez-cups | dbus-c++-glib |
| glibc-langpack-el | bogofilter | dconf-devel |
| glibc-langpack-eo | boost | dejagnu |
| glibc-langpack-es | boost-atomic | devhelp-devel |
| glibc-langpack-et | boost-chrono | device-mapper-event-devel |
| glibc-langpack-eu | boost-container | device-mapper-multipath-devel |
| glibc-langpack-fa | boost-context | docbook2X |
| glibc-langpack-ff | boost-coroutine | docbook5-schemas |
| glibc-langpack-fi | boost-date-time | docbook-style-dsssl |
| glibc-langpack-fil | boost-devel | docbook-utils |
| glibc-langpack-fo | boost-fiber | dotconf-devel |
| glibc-langpack-fr | boost-filesystem | dotnet5.0-build-reference-packages |
| glibc-langpack-fur | boost-graph | dotnet-build-reference-packages |
| glibc-langpack-fy | boost-iostreams | dotnet-sdk-3.1-source-built-artifacts |
| glibc-langpack-ga | boost-locale | dotnet-sdk-5.0-source-built-artifacts |
| glibc-langpack-gd | boost-log | dovecot-devel |
| glibc-langpack-gez | boost-math | doxygen |
| glibc-langpack-gl | boost-program-options | doxygen-doxywizard |
| glibc-langpack-gu | boost-random | doxygen-latex |
| glibc-langpack-gv | boost-regex | drpm-devel |
| glibc-langpack-ha | boost-serialization | dtc |
| glibc-langpack-hak | boost-signals | dwarves |
| glibc-langpack-he | boost-stacktrace | dyninst-devel |
| glibc-langpack-hi | boost-system | dyninst-doc |
| glibc-langpack-hif | boost-test | dyninst-static |
| glibc-langpack-hne | boost-thread | dyninst-testsuite |
| glibc-langpack-hr | boost-timer | easymock |
| glibc-langpack-hsb | boost-type_erasure | easymock-javadoc |
| glibc-langpack-ht | boost-wave | eglexternalplatform-devel |
| glibc-langpack-hu | bpftrace | eigen3-devel |
| glibc-langpack-hy | bpg-algeti-fonts | elfutils-devel-static |
| glibc-langpack-ia | bpg-chveulebrivi-fonts | elfutils-libelf-devel-static |
| glibc-langpack-id | bpg-classic-fonts | elinks |
| glibc-langpack-ig | bpg-courier-fonts | enca |
| glibc-langpack-ik | bpg-courier-s-fonts | enca-devel |
| glibc-langpack-is | bpg-dedaena-block-fonts | enchant2-devel |
| glibc-langpack-it | bpg-dejavu-sans-fonts | enchant-devel |
| glibc-langpack-iu | bpg-elite-fonts | evince-devel |
| glibc-langpack-ja | bpg-excelsior-caps-fonts | evolution-data-server-doc |
| glibc-langpack-ka | bpg-excelsior-condenced-fonts | evolution-data-server-perl |
| glibc-langpack-kab | bpg-excelsior-fonts | evolution-data-server-tests |
| glibc-langpack-kk | bpg-fonts-common | evolution-devel |
| glibc-langpack-kl | bpg-glaho-fonts | exec-maven-plugin |
| glibc-langpack-km | bpg-gorda-fonts | exec-maven-plugin-javadoc |
| glibc-langpack-kn | bpg-ingiri-fonts | execstack |
| glibc-langpack-ko | bpg-irubaqidze-fonts | exempi-devel |
| glibc-langpack-kok | bpg-mikhail-stephan-fonts | exiv2-devel |
| glibc-langpack-ks | bpg-mrgvlovani-caps-fonts | exiv2-doc |
| glibc-langpack-ku | bpg-mrgvlovani-fonts | felix-osgi-compendium |
| glibc-langpack-kw | bpg-nateli-caps-fonts | felix-osgi-compendium-javadoc |
| glibc-langpack-ky | bpg-nateli-condenced-fonts | felix-osgi-core |
| glibc-langpack-lb | bpg-nateli-fonts | felix-osgi-core-javadoc |
| glibc-langpack-lg | bpg-nino-medium-cond-fonts | felix-osgi-foundation |
| glibc-langpack-li | bpg-nino-medium-fonts | felix-osgi-foundation-javadoc |
| glibc-langpack-lij | bpg-sans-fonts | felix-parent |
| glibc-langpack-ln | bpg-sans-medium-fonts | felix-utils |
| glibc-langpack-lo | bpg-sans-modern-fonts | felix-utils-javadoc |
| glibc-langpack-lt | bpg-sans-regular-fonts | fftw-doc |
| glibc-langpack-lv | bpg-serif-fonts | file-devel |
| glibc-langpack-lzh | bpg-serif-modern-fonts | fipscheck-devel |
| glibc-langpack-mag | bpg-ucnobi-fonts | flac |
| glibc-langpack-mai | brasero | flac-devel |
| glibc-langpack-mfe | brasero-libs | flatpak-devel |
| glibc-langpack-mg | brasero-nautilus | flex-devel |
| glibc-langpack-mhr | brlapi | flite |
| glibc-langpack-mi | brlapi-java | flite-devel |
| glibc-langpack-miq | brltty | fltk-devel |
| glibc-langpack-mjw | brltty-at-spi2 | fontawesome-fonts-web |
| glibc-langpack-mk | brltty-docs | fontconfig-devel-doc |
| glibc-langpack-ml | brltty-dracut | fontforge |
| glibc-langpack-mn | brltty-espeak-ng | fontpackages-devel |
| glibc-langpack-mni | brltty-xw | forge-parent |
| glibc-langpack-mr | byacc | freeipmi-devel |
| glibc-langpack-ms | byteman | freerdp-devel |
| glibc-langpack-mt | byteman-javadoc | frei0r-devel |
| glibc-langpack-my | c2esp | fusesource-pom |
| glibc-langpack-nan | cairo-devel | fuse-sshfs |
| glibc-langpack-nb | cairo-gobject-devel | gamin-devel |
| glibc-langpack-nds | cairomm | gcc-plugin-devel |
| glibc-langpack-ne | cargo-vendor | gcc-toolset-10-gcc-plugin-devel |
| glibc-langpack-nhn | cdparanoia | gcc-toolset-9-dyninst-devel |
| glibc-langpack-niu | cdparanoia-libs | gcc-toolset-9-dyninst-doc |
| glibc-langpack-nl | cdrdao | gcc-toolset-9-dyninst-static |
| glibc-langpack-nn | celt051 | gcc-toolset-9-dyninst-testsuite |
| glibc-langpack-nr | certmonger | gcc-toolset-9-gcc-plugin-devel |
| glibc-langpack-nso | cgdcbxd | gc-devel |
| glibc-langpack-oc | chan | GConf2-devel |
| glibc-langpack-om | check | gdk-pixbuf2-xlib |
| glibc-langpack-or | check-devel | gdk-pixbuf2-xlib-devel |
| glibc-langpack-os | cheese | gegl04-devel |
| glibc-langpack-pa | cheese-libs | geoclue2-devel |
| glibc-langpack-pap | chrome-gnome-shell | geronimo-annotation |
| glibc-langpack-pl | cim-schema | geronimo-annotation-javadoc |
| glibc-langpack-ps | cjose | geronimo-jms |
| glibc-langpack-pt | cjose-devel | geronimo-jms-javadoc |
| glibc-langpack-quz | cldr-emoji-annotation | geronimo-jpa |
| glibc-langpack-raj | clevis | geronimo-jpa-javadoc |
| glibc-langpack-ro | clevis-dracut | geronimo-parent-poms |
| glibc-langpack-ru | clevis-luks | gflags |
| glibc-langpack-rw | clevis-systemd | gflags-devel |
| glibc-langpack-sa | clevis-udisks2 | ghostscript-doc |
| glibc-langpack-sah | cloud-init | ghostscript-tools-dvipdf |
| glibc-langpack-sat | cloud-utils-growpart | ghostscript-tools-fonts |
| glibc-langpack-sc | clucene-contribs-lib | ghostscript-tools-printing |
| glibc-langpack-sd | clucene-core | ghostscript-x11 |
| glibc-langpack-se | clutter | giflib-devel |
| glibc-langpack-sgs | clutter-gst2 | gjs-devel |
| glibc-langpack-shn | clutter-gst3 | glade-devel |
| glibc-langpack-shs | clutter-gtk | glassfish-annotation-api |
| glibc-langpack-si | cmake-doc | glassfish-annotation-api-javadoc |
| glibc-langpack-sid | cmake-gui | glassfish-el |
| glibc-langpack-sk | cockpit-composer | glassfish-el-api |
| glibc-langpack-sl | cockpit-dashboard | glassfish-el-javadoc |
| glibc-langpack-sm | cockpit-machines | glassfish-jaxb-bom |
| glibc-langpack-so | cockpit-packagekit | glassfish-jaxb-bom-ext |
| glibc-langpack-sq | cockpit-pcp | glassfish-jaxb-codemodel-parent |
| glibc-langpack-sr | cockpit-session-recording | glassfish-jaxb-external-parent |
| glibc-langpack-ss | cockpit-storaged | glassfish-jaxb-parent |
| glibc-langpack-st | cogl | glassfish-jaxb-runtime-parent |
| glibc-langpack-sv | colord | glassfish-jaxb-txw-parent |
| glibc-langpack-sw | colord-gtk | glassfish-jsp-api |
| glibc-langpack-szl | color-filesystem | glassfish-jsp-api-javadoc |
| glibc-langpack-ta | compat-exiv2-026 | glassfish-legal |
| glibc-langpack-tcy | compat-libgfortran-48 | glassfish-master-pom |
| glibc-langpack-te | compat-libpthread-nonshared | glassfish-servlet-api |
| glibc-langpack-tg | compat-libtiff3 | glassfish-servlet-api-javadoc |
| glibc-langpack-th | compat-locales-sap | glew-devel |
| glibc-langpack-the | composer-cli | glib2-doc |
| glibc-langpack-ti | container-exception-logger | glib2-static |
| glibc-langpack-tig | containers-common | glibc-benchtests |
| glibc-langpack-tk | container-selinux | glibc-nss-devel |
| glibc-langpack-tl | convmv | glibmm24-devel |
| glibc-langpack-tn | coreos-installer | glibmm24-doc |
| glibc-langpack-to | coreos-installer-bootinfra | glm-devel |
| glibc-langpack-tpi | corosynclib | glm-doc |
| glibc-langpack-tr | crash | glog |
| glibc-langpack-ts | crash-gcore-command | glog-devel |
| glibc-langpack-tt | crash-ptdump-command | glusterfs-api-devel |
| glibc-langpack-ug | crash-trace-command | glusterfs-devel |
| glibc-langpack-uk | createrepo_c | gmock |
| glibc-langpack-unm | createrepo_c-devel | gmock-devel |
| glibc-langpack-ur | createrepo_c-libs | gnome-bluetooth-libs-devel |
| glibc-langpack-uz | cryptsetup-devel | gnome-common |
| glibc-langpack-ve | cscope | gnome-menus-devel |
| glibc-langpack-vi | ctags | gnome-software-devel |
| glibc-langpack-wa | culmus-aharoni-clm-fonts | gnu-efi |
| glibc-langpack-wae | culmus-caladings-clm-fonts | gnu-efi-devel |
| glibc-langpack-wal | culmus-david-clm-fonts | gnuplot-doc |
| glibc-langpack-wo | culmus-drugulin-clm-fonts | gobject-introspection-devel |
| glibc-langpack-xh | culmus-ellinia-clm-fonts | go-compilers-golang-compiler |
| glibc-langpack-yi | culmus-fonts-common | google-guice |
| glibc-langpack-yo | culmus-frank-ruehl-clm-fonts | google-guice-javadoc |
| glibc-langpack-yue | culmus-hadasim-clm-fonts | google-noto-sans-cjk-jp-fonts |
| glibc-langpack-yuw | culmus-keteryg-fonts | google-roboto-slab-fonts |
| glibc-langpack-zh | culmus-miriam-clm-fonts | gperf |
| glibc-langpack-zu | culmus-miriam-mono-clm-fonts | gpgmepp-devel |
| glusterfs | culmus-nachlieli-clm-fonts | graphviz-devel |
| glusterfs-client-xlators | culmus-shofar-fonts | graphviz-doc |
| glusterfs-fuse | culmus-simple-clm-fonts | graphviz-gd |
| glusterfs-libs | culmus-stamashkenaz-clm-fonts | graphviz-python3 |
| glusterfs-rdma | culmus-stamsefarad-clm-fonts | grilo-devel |
| gmp-c++ | culmus-yehuda-clm-fonts | groff |
| gmp-devel | CUnit | gsm-devel |
| gpgmepp | cups-devel | gspell-devel |
| grub2-common | cups-lpd | gspell-doc |
| grub2-efi-aa64-modules | cups-pk-helper | gssdp-devel |
| grub2-efi-ia32 | custodia | gssdp-docs |
| grub2-efi-ia32-cdboot | cyrus-imapd | gstreamer1-plugins-bad-free-devel |
| grub2-efi-ia32-modules | cyrus-imapd-utils | gtest |
| grub2-efi-x64 | cyrus-imapd-vzic | gtest-devel |
| grub2-efi-x64-cdboot | cyrus-sasl-sql | gtk-doc |
| grub2-efi-x64-modules | daxctl-devel | gtkmm24-devel |
| grub2-pc | daxio | gtkmm24-docs |
| grub2-pc-modules | dbus-devel | gtkmm30-devel |
| grub2-ppc64le-modules | dbus-glib-devel | gtkmm30-doc |
| grub2-tools | dbus-x11 | gtksourceview3-devel |
| grub2-tools-efi | dconf-editor | gtkspell3-devel |
| grub2-tools-extra | dcraw | gtkspell-devel |
| grub2-tools-minimal | dejavu-lgc-sans-fonts | guava20 |
| grubby | desktop-file-utils | guava20-javadoc |
| gssproxy | devhelp | guava20-testlib |
| hdparm | devhelp-libs | guice-assistedinject |
| hwloc | dialog | guice-bom |
| hwloc-libs | diffstat | guice-extensions |
| ibacm | directory-maven-plugin | guice-grapher |
| icu | directory-maven-plugin-javadoc | guice-jmx |
| ima-evm-utils0 | dirsplit | guice-jndi |
| infiniband-diags | dleyna-connector-dbus | guice-multibindings |
| integritysetup | dleyna-core | guice-parent |
| intel-cmt-cat | dleyna-renderer | guice-servlet |
| iotop | dleyna-server | guice-testlib |
| iproute-tc | dnf-plugin-spacewalk | guice-throwingproviders |
| iprutils | dnsmasq-utils | guile-devel |
| ipset-service | dnssec-trigger | gupnp-devel |
| iptables-arptables | dnssec-trigger-panel | gupnp-igd-devel |
| iptables-devel | docbook-dtds | hamcrest |
| iptables-ebtables | docbook-style-xsl | hamcrest-core |
| iptables-services | dotconf | hamcrest-demo |
| iptables-utils | dovecot | hamcrest-javadoc |
| iptraf-ng | dovecot-mysql | hawtjni |
| iptstate | dovecot-pgsql | hawtjni-javadoc |
| iputils-ninfod | dovecot-pigeonhole | hawtjni-runtime |
| irqbalance | dpdk | hesiod-devel |
| isns-utils | dpdk-devel | httpcomponents-client |
| isns-utils-devel | dpdk-doc | httpcomponents-client-cache |
| iw | dpdk-tools | httpcomponents-client-javadoc |
| iwl1000-firmware | driverctl | httpcomponents-core |
| iwl100-firmware | dropwatch | httpcomponents-core-javadoc |
| iwl105-firmware | drpm | httpcomponents-project |
| iwl135-firmware | dvd+rw-tools | http-parser-devel |
| iwl2000-firmware | dyninst | hwloc-devel |
| iwl2030-firmware | eclipse-ecf-core | hyphen-devel |
| iwl3160-firmware | eclipse-ecf-runtime | ibus-devel |
| iwl3945-firmware | eclipse-emf-core | ibus-devel-docs |
| iwl4965-firmware | eclipse-emf-runtime | ibus-table-devel |
| iwl5000-firmware | eclipse-emf-xsd | ibus-table-tests |
| iwl5150-firmware | eclipse-equinox-osgi | ibus-typing-booster-tests |
| iwl6000-firmware | eclipse-jdt | ilmbase-devel |
| iwl6000g2a-firmware | eclipse-p2-discovery | ima-evm-utils-devel |
| iwl6000g2b-firmware | eclipse-pde | imake |
| iwl6050-firmware | eclipse-platform | infiniband-diags |
| iwl7260-firmware | eclipse-swt | infiniband-diags-compat |
| iwpmd | edk2-ovmf | infiniband-diags-devel |
| jimtcl | ee4j-parent | infiniband-diags-devel-static |
| kabi-dw | egl-wayland | intel-cmt-cat-devel |
| kernel | elfutils-debuginfod-client-devel | iproute-devel |
| kernel-abi-stablelists | emacs | ipset-devel |
| kernel-abi-whitelists | emacs-common | irssi-devel |
| kernel-core | emacs-lucid | iscsi-initiator-utils-devel |
| kernel-cross-headers | emacs-nox | isl-devel |
| kernel-debug | emacs-terminal | iso-codes-devel |
| kernel-debug-core | emoji-picker | isorelax |
| kernel-debug-devel | enchant2 | isorelax-javadoc |
| kernel-debug-modules | enscript | istack-commons |
| kernel-debug-modules-extra | eog | ivy-local |
| kernel-devel | esc | jakarta-commons-httpclient |
| kernel-doc | espeak-ng | jakarta-commons-httpclient-demo |
| kernel-modules | eth-tools-basic | jakarta-commons-httpclient-javadoc |
| kernel-modules-extra | eth-tools-fastfabric | jakarta-commons-httpclient-manual |
| kernel-tools | evemu | jakarta-oro |
| kernel-tools-libs | evemu-libs | jakarta-oro-javadoc |
| kexec-tools | evince | jansi |
| keyutils | evince-browser-plugin | jansi-javadoc |
| kmod-kvdo | evince-libs | jansi-native |
| kmod-redhat-atlantic | evince-nautilus | jansi-native-javadoc |
| kmod-redhat-bnxt_en | evolution | jasper-devel |
| kmod-redhat-btusb | evolution-bogofilter | java-11-openjdk-demo-fastdebug |
| kmod-redhat-btusb-firmware | evolution-data-server | java-11-openjdk-demo-slowdebug |
| kmod-redhat-ice | evolution-data-server-devel | java-11-openjdk-devel-fastdebug |
| kmod-redhat-ice-firmware | evolution-data-server-langpacks | java-11-openjdk-devel-slowdebug |
| kmod-redhat-ionic | evolution-ews | java-11-openjdk-fastdebug |
| kmod-redhat-iwlwifi | evolution-ews-langpacks | java-11-openjdk-headless-fastdebug |
| kmod-redhat-iwlwifi-devel | evolution-help | java-11-openjdk-headless-slowdebug |
| kmod-redhat-iwlwifi-firmware | evolution-langpacks | java-11-openjdk-jmods-fastdebug |
| kmod-redhat-mlx5_core | evolution-mapi | java-11-openjdk-jmods-slowdebug |
| kmod-redhat-mlx5_core-devel | evolution-mapi-langpacks | java-11-openjdk-slowdebug |
| kmod-redhat-mpt3sas | evolution-pst | java-11-openjdk-src-fastdebug |
| kmod-redhat-oracleasm | evolution-spamassassin | java-11-openjdk-src-slowdebug |
| kmod-redhat-oracleasm-kernel_4_18_0_240 | exchange-bmc-os-info | java-11-openjdk-static-libs-fastdebug |
| kmod-redhat-oracleasm-kernel_4_18_0_240_14_1 | exempi | java-11-openjdk-static-libs-slowdebug |
| kpatch | exiv2 | java-17-openjdk-demo-fastdebug |
| kpatch-dnf | exiv2-libs | java-17-openjdk-demo-slowdebug |
| kpatch-patch-4_18_0-147 | fabtests | java-17-openjdk-devel-fastdebug |
| kpatch-patch-4_18_0-147_0_2 | fapolicyd | java-17-openjdk-devel-slowdebug |
| kpatch-patch-4_18_0-147_0_3 | fapolicyd-selinux | java-17-openjdk-fastdebug |
| kpatch-patch-4_18_0-147_3_1 | farstream02 | java-17-openjdk-headless-fastdebug |
| kpatch-patch-4_18_0-147_5_1 | felix-gogo-command | java-17-openjdk-headless-slowdebug |
| kpatch-patch-4_18_0-193 | felix-gogo-runtime | java-17-openjdk-jmods-fastdebug |
| kpatch-patch-4_18_0-193_1_2 | felix-gogo-shell | java-17-openjdk-jmods-slowdebug |
| kpatch-patch-4_18_0-193_13_2 | felix-scr | java-17-openjdk-slowdebug |
| kpatch-patch-4_18_0-193_14_3 | fence-agents-all | java-17-openjdk-src-fastdebug |
| kpatch-patch-4_18_0-193_19_1 | fence-agents-amt-ws | java-17-openjdk-src-slowdebug |
| kpatch-patch-4_18_0-193_28_1 | fence-agents-apc | java-17-openjdk-static-libs-fastdebug |
| kpatch-patch-4_18_0-193_6_3 | fence-agents-apc-snmp | java-17-openjdk-static-libs-slowdebug |
| kpatch-patch-4_18_0-305 | fence-agents-bladecenter | java-1.8.0-openjdk-accessibility-fastdebug |
| kpatch-patch-4_18_0-305_10_2 | fence-agents-brocade | java-1.8.0-openjdk-accessibility-slowdebug |
| kpatch-patch-4_18_0-305_12_1 | fence-agents-cisco-mds | java-1.8.0-openjdk-demo-fastdebug |
| kpatch-patch-4_18_0-305_17_1 | fence-agents-cisco-ucs | java-1.8.0-openjdk-demo-slowdebug |
| kpatch-patch-4_18_0-305_19_1 | fence-agents-common | java-1.8.0-openjdk-devel-fastdebug |
| kpatch-patch-4_18_0-305_25_1 | fence-agents-compute | java-1.8.0-openjdk-devel-slowdebug |
| kpatch-patch-4_18_0-305_3_1 | fence-agents-drac5 | java-1.8.0-openjdk-fastdebug |
| kpatch-patch-4_18_0-305_7_1 | fence-agents-eaton-snmp | java-1.8.0-openjdk-headless-fastdebug |
| kpatch-patch-4_18_0-348 | fence-agents-emerson | java-1.8.0-openjdk-headless-slowdebug |
| kpatch-patch-4_18_0-348_2_1 | fence-agents-eps | java-1.8.0-openjdk-slowdebug |
| kpatch-patch-4_18_0-348_7_1 | fence-agents-heuristics-ping | java-1.8.0-openjdk-src-fastdebug |
| krb5-pkinit | fence-agents-hpblade | java-1.8.0-openjdk-src-slowdebug |
| krb5-server | fence-agents-ibmblade | javacc |
| krb5-server-ldap | fence-agents-ifmib | javacc-demo |
| ksc | fence-agents-ilo2 | javacc-javadoc |
| ldb-tools | fence-agents-ilo-moonshot | javacc-manual |
| ledmon | fence-agents-ilo-mp | javacc-maven-plugin |
| libacl-devel | fence-agents-ilo-ssh | javacc-maven-plugin-javadoc |
| libaio-devel | fence-agents-intelmodular | java_cup |
| libappstream-glib | fence-agents-ipdu | java_cup-javadoc |
| libatomic-static | fence-agents-ipmilan | java_cup-manual |
| libattr-devel | fence-agents-kdump | javamail |
| libbabeltrace | fence-agents-lpar | javamail-javadoc |
| libbasicobjects | fence-agents-mpath | javapackages-filesystem |
| libblkid-devel | fence-agents-redfish | javapackages-local |
| libcap-devel | fence-agents-rhevm | javapackages-tools |
| libcap-ng-python3 | fence-agents-rsa | javassist |
| libcap-ng-utils | fence-agents-rsb | javassist-javadoc |
| libcgroup-pam | fence-agents-sbd | jaxen |
| libcgroup-tools | fence-agents-scsi | jaxen-demo |
| libcollection | fence-agents-virsh | jaxen-javadoc |
| libcomps-devel | fence-agents-vmware-rest | jbigkit-devel |
| libconfig | fence-agents-vmware-soap | jboss-interceptors-1.2-api |
| libcurl-minimal | fence-agents-wti | jboss-interceptors-1.2-api-javadoc |
| libdaemon | fence-virt | jboss-parent |
| libdhash | fence-virtd | jcl-over-slf4j |
| libdmmp | fence-virtd-libvirt | jdepend |
| liberation-fonts | fence-virtd-multicast | jdepend-demo |
| liberation-narrow-fonts | fence-virtd-serial | jdependency |
| liberation-sans-fonts | fence-virtd-tcp | jdependency-javadoc |
| liberation-serif-fonts | fetchmail | jdepend-javadoc |
| libertas-sd8686-firmware | fftw-static | jdom |
| libertas-sd8787-firmware | file-roller | jdom2 |
| libertas-usb8388-firmware | file-roller-nautilus | jdom2-javadoc |
| libertas-usb8388-olpc-firmware | fio | jdom-demo |
| libevent-doc | firefox | jdom-javadoc |
| libfabric | firewall-applet | jflex |
| libfdisk-devel | firewall-config | jflex-javadoc |
| libgcab1 | flatpak | jimtcl-devel |
| libgomp-offload-nvptx | flatpak-builder | jline |
| libgudev | flatpak-libs | jline-javadoc |
| libhbaapi | flatpak-selinux | jna |
| libhbalinux | flatpak-session-helper | jq-devel |
| libhbalinux-devel | flatpak-xdg-utils | jsch |
| libhugetlbfs | flex | jsch-javadoc |
| libhugetlbfs-devel | flex-doc | json-c-devel |
| libhugetlbfs-utils | fltk | json-c-doc |
| libibumad | flute | jsoup |
| libibverbs-utils | fonts-tweak-tool | jsoup-javadoc |
| libical | foomatic | jsr-305 |
| libicu-doc | foomatic-db | jsr-305-javadoc |
| libini_config | foomatic-db-filesystem | js-uglify |
| libipa_hbac | foomatic-db-ppds | jtidy |
| libitm | fprintd | jtidy-javadoc |
| libkeepalive | fprintd-pam | Judy-devel |
| libldb | freeglut | jul-to-slf4j |
| libldb-devel | freeglut-devel | junit |
| liblsan | freeradius | junit-javadoc |
| libmbim | freeradius-devel | junit-manual |
| libmbim-utils | freeradius-doc | jvnet-parent |
| libmicrohttpd | freeradius-krb5 | jzlib |
| libndp | freeradius-ldap | jzlib-demo |
| libnetfilter_cthelper | freeradius-mysql | jzlib-javadoc |
| libnetfilter_cttimeout | freeradius-perl | kernel-tools-libs-devel |
| libnetfilter_queue | freeradius-postgresql | keybinder3-devel |
| libnfsidmap | freeradius-rest | keybinder3-doc |
| libnl3-cli | freeradius-sqlite | kmod-devel |
| libnl3-devel | freeradius-unixODBC | ladspa |
| libnl3-doc | freeradius-utils | ladspa-devel |
| libnsl | freerdp | lame-devel |
| libpsm2 | freerdp-libs | lapack-devel |
| libpsm2-compat | frei0r-plugins | lapack-static |
| libqb | frei0r-plugins-opencv | lasso-devel |
| libqb-devel | fribidi-devel | latex2html |
| libqmi | frr | lcms2-devel |
| libqmi-utils | frr-contrib | ldns-devel |
| librabbitmq | fstrm-devel | lensfun |
| librdmacm | ftp | lensfun-devel |
| librdmacm-utils | galera | leptonica |
| libref_array | gavl | leptonica-devel |
| libsecret-devel | gcc-offload-nvptx | libao-devel |
| libsmartcols-devel | gcc-toolset-10 | libappindicator-gtk3-devel |
| libsmbclient | gcc-toolset-10-annobin | libappstream-glib-devel |
| libsmbios | gcc-toolset-10-binutils | libarchive-devel |
| libsss_autofs | gcc-toolset-10-binutils-devel | libasyncns-devel |
| libsss_certmap | gcc-toolset-10-build | libatasmart-devel |
| libsss_idmap | gcc-toolset-10-dwz | libatomic_ops-devel |
| libsss_nss_idmap | gcc-toolset-10-dyninst | libbabeltrace-devel |
| libsss_simpleifp | gcc-toolset-10-dyninst-devel | libbasicobjects-devel |
| libsss_sudo | gcc-toolset-10-elfutils | libblockdev-crypto-devel |
| libstemmer | gcc-toolset-10-elfutils-debuginfod-client | libblockdev-devel |
| libstoragemgmt | gcc-toolset-10-elfutils-debuginfod-client-devel | libblockdev-fs-devel |
| libstoragemgmt-arcconf-plugin | gcc-toolset-10-elfutils-devel | libblockdev-loop-devel |
| libstoragemgmt-hpsa-plugin | gcc-toolset-10-elfutils-libelf | libblockdev-lvm-devel |
| libstoragemgmt-local-plugin | gcc-toolset-10-elfutils-libelf-devel | libblockdev-mdraid-devel |
| libstoragemgmt-megaraid-plugin | gcc-toolset-10-elfutils-libs | libblockdev-part-devel |
| libstoragemgmt-netapp-plugin | gcc-toolset-10-gcc | libblockdev-swap-devel |
| libstoragemgmt-nfs-plugin | gcc-toolset-10-gcc-c++ | libblockdev-utils-devel |
| libstoragemgmt-nfs-plugin-clibs | gcc-toolset-10-gcc-gdb-plugin | libblockdev-vdo-devel |
| libstoragemgmt-nstor-plugin | gcc-toolset-10-gcc-gfortran | libbpf-devel |
| libstoragemgmt-smis-plugin | gcc-toolset-10-gdb | libbpf-static |
| libstoragemgmt-udev | gcc-toolset-10-gdb-doc | libburn-devel |
| libtalloc-devel | gcc-toolset-10-gdb-gdbserver | libbytesize-devel |
| libtdb | gcc-toolset-10-libasan-devel | libcacard-devel |
| libtdb-devel | gcc-toolset-10-libatomic-devel | libcdio-devel |
| libteam | gcc-toolset-10-libitm-devel | libcdio-paranoia-devel |
| libteam-doc | gcc-toolset-10-liblsan-devel | libcephfs2 |
| libtevent | gcc-toolset-10-libquadmath-devel | libcephfs-devel |
| libtevent-devel | gcc-toolset-10-libstdc++-devel | libchamplain |
| libtirpc-devel | gcc-toolset-10-libstdc++-docs | libchamplain-devel |
| libtsan | gcc-toolset-10-libtsan-devel | libchamplain-gtk |
| libusb | gcc-toolset-10-libubsan-devel | libcmocka |
| libusbx-devel | gcc-toolset-10-ltrace | libcmocka-devel |
| libusbx-devel-doc | gcc-toolset-10-make | libcollection-devel |
| libvarlink | gcc-toolset-10-make-devel | libcomps-devel |
| libvarlink-util | gcc-toolset-10-perftools | libconfig-devel |
| libverto-libevent | gcc-toolset-10-runtime | libcroco-devel |
| libwbclient | gcc-toolset-10-strace | libdaemon-devel |
| libxmlb | gcc-toolset-10-systemtap | libdap |
| libzstd-devel | gcc-toolset-10-systemtap-client | libdap-devel |
| linux-firmware | gcc-toolset-10-systemtap-devel | libdatrie-devel |
| lksctp-tools-devel | gcc-toolset-10-systemtap-initscript | libdazzle-devel |
| lksctp-tools-doc | gcc-toolset-10-systemtap-runtime | libdb-cxx |
| lldpad | gcc-toolset-10-systemtap-sdt-devel | libdb-cxx-devel |
| lmdb-libs | gcc-toolset-10-systemtap-server | libdb-devel-doc |
| lm_sensors | gcc-toolset-10-toolchain | libdb-sql |
| lm_sensors-devel | gcc-toolset-10-valgrind | libdb-sql-devel |
| lockdev | gcc-toolset-10-valgrind-devel | libdbusmenu-devel |
| logwatch | gcc-toolset-11 | libdbusmenu-doc |
| lrzsz | gcc-toolset-11-annobin-annocheck | libdbusmenu-gtk3-devel |
| lsscsi | gcc-toolset-11-annobin-docs | libdnet-devel |
| lvm2-dbusd | gcc-toolset-11-annobin-plugin-gcc | libdnf-devel |
| lvm2-lockd | gcc-toolset-11-binutils | libdv-devel |
| lzo-devel | gcc-toolset-11-binutils-devel | libdvdread-devel |
| lzo-minilzo | gcc-toolset-11-build | libdwarf-devel |
| lzop | gcc-toolset-11-dwz | libdwarf-static |
| make-devel | gcc-toolset-11-dyninst | libdwarf-tools |
| man-db-cron | gcc-toolset-11-dyninst-devel | libdwarves1 |
| man-pages | gcc-toolset-11-elfutils | libecpg-devel |
| mcelog | gcc-toolset-11-elfutils-debuginfod-client | libEMF |
| mcstrans | gcc-toolset-11-elfutils-debuginfod-client-devel | libEMF-devel |
| mdadm | gcc-toolset-11-elfutils-devel | libeot |
| memstrack | gcc-toolset-11-elfutils-libelf | libepubgen-devel |
| memtest86+ | gcc-toolset-11-elfutils-libelf-devel | libetonyek-devel |
| microcode_ctl | gcc-toolset-11-elfutils-libs | libevdev-devel |
| minicom | gcc-toolset-11-gcc | libfabric-devel |
| mksh | gcc-toolset-11-gcc-c++ | libfdt |
| mlocate | gcc-toolset-11-gcc-gdb-plugin | libfdt-devel |
| mobile-broadband-provider-info | gcc-toolset-11-gcc-gfortran | libfontenc-devel |
| ModemManager | gcc-toolset-11-gcc-plugin-devel | libgee-devel |
| ModemManager-glib | gcc-toolset-11-gdb | libgexiv2-devel |
| mokutil | gcc-toolset-11-gdb-doc | libgit2-devel |
| mtools | gcc-toolset-11-gdb-gdbserver | libgit2-glib-devel |
| mtr | gcc-toolset-11-libasan-devel | libGLEW |
| ncurses-term | gcc-toolset-11-libatomic-devel | libgnomekbd-devel |
| ndctl | gcc-toolset-11-libgccjit | libgphoto2-devel |
| ndctl-libs | gcc-toolset-11-libgccjit-devel | libgpod-devel |
| netconsole-service | gcc-toolset-11-libgccjit-docs | libgpod-doc |
| netlabel_tools | gcc-toolset-11-libitm-devel | libgs-devel |
| NetworkManager | gcc-toolset-11-liblsan-devel | libgsf-devel |
| NetworkManager-adsl | gcc-toolset-11-libquadmath-devel | libgtop2-devel |
| NetworkManager-bluetooth | gcc-toolset-11-libstdc++-devel | libgudev-devel |
| NetworkManager-config-connectivity-redhat | gcc-toolset-11-libstdc++-docs | libgusb-devel |
| NetworkManager-config-server | gcc-toolset-11-libtsan-devel | libgxps-devel |
| NetworkManager-dispatcher-routing-rules | gcc-toolset-11-libubsan-devel | libhbaapi-devel |
| NetworkManager-libnm | gcc-toolset-11-ltrace | libIDL |
| NetworkManager-ovs | gcc-toolset-11-make | libIDL-devel |
| NetworkManager-ppp | gcc-toolset-11-make-devel | libidn2-devel |
| NetworkManager-team | gcc-toolset-11-perftools | libidn-devel |
| NetworkManager-tui | gcc-toolset-11-runtime | libiec61883-devel |
| NetworkManager-wifi | gcc-toolset-11-strace | libimobiledevice-devel |
| NetworkManager-wwan | gcc-toolset-11-systemtap | libindicator-gtk3-devel |
| network-scripts-team | gcc-toolset-11-systemtap-client | libini_config-devel |
| newt | gcc-toolset-11-systemtap-devel | libinput-devel |
| nfs4-acl-tools | gcc-toolset-11-systemtap-initscript | libisoburn-devel |
| nfs-utils | gcc-toolset-11-systemtap-runtime | libisofs-devel |
| nscd | gcc-toolset-11-systemtap-sdt-devel | libknet1 |
| nss_db | gcc-toolset-11-systemtap-server | libknet1-devel |
| nss_nis | gcc-toolset-11-toolchain | libksba-devel |
| ntsysv | gcc-toolset-11-valgrind | liblangtag-devel |
| numactl | gcc-toolset-11-valgrind-devel | liblangtag-doc |
| numactl-devel | gcc-toolset-9 | liblangtag-gobject |
| numactl-libs | gcc-toolset-9-annobin | liblockfile-devel |
| numad | gcc-toolset-9-binutils | libmad-devel |
| numatop | gcc-toolset-9-binutils-devel | libmaxminddb-devel |
| nvme-cli | gcc-toolset-9-build | libmicrohttpd-devel |
| nvmetcli | gcc-toolset-9-dwz | libmicrohttpd-doc |
| opa-address-resolution | gcc-toolset-9-dyninst | libmnl-devel |
| opa-basic-tools | gcc-toolset-9-elfutils | libmodulemd-devel |
| opa-fastfabric | gcc-toolset-9-elfutils-devel | libmount-devel |
| opa-fm | gcc-toolset-9-elfutils-libelf | libmpcdec-devel |
| opa-libopamgt | gcc-toolset-9-elfutils-libelf-devel | libmpc-devel |
| opencryptoki | gcc-toolset-9-elfutils-libs | libmspack-devel |
| opencryptoki-icsftok | gcc-toolset-9-gcc | libmtp-devel |
| opencryptoki-libs | gcc-toolset-9-gcc-c++ | libmusicbrainz5-devel |
| opencryptoki-swtok | gcc-toolset-9-gcc-gdb-plugin | libnet-devel |
| opencryptoki-tpmtok | gcc-toolset-9-gcc-gfortran | libnetfilter_conntrack-devel |
| openhpi | gcc-toolset-9-gdb | libnetfilter_queue-devel |
| openhpi-libs | gcc-toolset-9-gdb-doc | libnfnetlink-devel |
| OpenIPMI | gcc-toolset-9-gdb-gdbserver | libnfsidmap-devel |
| OpenIPMI-lanserv | gcc-toolset-9-libasan-devel | libnftnl-devel |
| OpenIPMI-libs | gcc-toolset-9-libatomic-devel | libnghttp2-devel |
| OpenIPMI-perl | gcc-toolset-9-libitm-devel | libnice-devel |
| opensc | gcc-toolset-9-liblsan-devel | libnma-devel |
| opensm | gcc-toolset-9-libquadmath-devel | libnsl2-devel |
| opensm-libs | gcc-toolset-9-libstdc++-devel | libodfgen-devel |
| openssh-cavs | gcc-toolset-9-libstdc++-docs | libogg-devel |
| openssh-keycat | gcc-toolset-9-libtsan-devel | libogg-devel-docs |
| openssh-ldap | gcc-toolset-9-libubsan-devel | liboggz |
| openssl-ibmpkcs11 | gcc-toolset-9-ltrace | libopenraw-devel |
| openssl-perl | gcc-toolset-9-make | libopenraw-gnome |
| os-prober | gcc-toolset-9-make-devel | libopenraw-gnome-devel |
| p11-kit-devel | gcc-toolset-9-perftools | libpaper-devel |
| p11-kit-server | gcc-toolset-9-runtime | libpath_utils-devel |
| pam_cifscreds | gcc-toolset-9-strace | libpcap-devel |
| pam-devel | gcc-toolset-9-systemtap | libpciaccess-devel |
| pam_ssh_agent_auth | gcc-toolset-9-systemtap-client | libpeas-devel |
| parted | gcc-toolset-9-systemtap-devel | libpfm-static |
| pciutils-devel | gcc-toolset-9-systemtap-initscript | libplist-devel |
| pcsc-lite | gcc-toolset-9-systemtap-runtime | libpmemblk-debug |
| pcsc-lite-ccid | gcc-toolset-9-systemtap-sdt-devel | libpmem-debug |
| pcsc-lite-doc | gcc-toolset-9-systemtap-server | libpmemlog-debug |
| perf | gcc-toolset-9-toolchain | libpmemobj-debug |
| perftest | gcc-toolset-9-valgrind | libpmempool-debug |
| perl-Algorithm-Diff | gcc-toolset-9-valgrind-devel | libproxy-devel |
| perl-Archive-Tar | gcr | libpsl-devel |
| perl-Date-Manip | gcr-devel | libpsm2-devel |
| perl-Parse-Yapp | gdb-doc | libpurple-devel |
| perl-Sys-CPU | gdk-pixbuf2-devel | libpwquality-devel |
| perl-Sys-MemInfo | gdm | libqhull |
| perl-Text-Diff | gedit | libqhull_p |
| policycoreutils-dbus | gedit-plugin-bookmarks | libqhull_r |
| policycoreutils-devel | gedit-plugin-bracketcompletion | libquvi-devel |
| policycoreutils-newrole | gedit-plugin-codecomment | librabbitmq-devel |
| policycoreutils-restorecond | gedit-plugin-colorpicker | librados-devel |
| polkit-devel | gedit-plugin-colorschemer | libradosstriper1 |
| polkit-docs | gedit-plugin-commander | libradosstriper-devel |
| popt-devel | gedit-plugin-drawspaces | libraw1394-devel |
| portreserve | gedit-plugin-findinfiles | LibRaw-devel |
| ppp | gedit-plugin-joinlines | librbd-devel |
| prefixdevname | gedit-plugin-multiedit | librdkafka-devel |
| procps-ng-i18n | gedit-plugins | libref_array-devel |
| psacct | gedit-plugins-data | libreoffice-sdk |
| ps_mem | gedit-plugin-smartspaces | libreoffice-sdk-doc |
| python3-avahi | gedit-plugin-terminal | librepo-devel |
| python3-boom | gedit-plugin-textsize | librevenge-devel |
| python3-configobj | gedit-plugin-translate | librhsm-devel |
| python3-configshell | gedit-plugin-wordcompletion | librpmem-debug |
| python3-dnf-plugin-post-transaction-actions | gegl | librx |
| python3-dnf-plugin-versionlock | gegl04 | librx-devel |
| python3-firewall | genisoimage | libsamplerate-devel |
| python3-iscsi-initiator-utils | geoclue2 | libsass |
| python3-jwt | geoclue2-demos | libsass-devel |
| python3-kmod | geoclue2-libs | libseccomp-devel |
| python3-ldb | geocode-glib | libsemanage-devel |
| python3-libipa_hbac | geocode-glib-devel | libsepol-static |
| python3-libnl3 | geoipupdate | libshout-devel |
| python3-libproxy | gfbgraph | libsigc++20-devel |
| python3-libsss_nss_idmap | ghostscript-x11 | libsigc++20-doc |
| python3-libstoragemgmt | gimp | libsigsegv-devel |
| python3-libstoragemgmt-clibs | gimp-devel | libsmbclient-devel |
| python3-libuser | gimp-devel-tools | libsmi-devel |
| python3-linux-procfs | gimp-libs | libsndfile-devel |
| python3-magic | git-all | libsolv-devel |
| python3-nftables | git-credential-libsecret | libsolv-tools |
| python3-oauthlib | git-daemon | libspectre-devel |
| python3-openipmi | git-email | libsrtp-devel |
| python3-perf | git-gui | libssh2-devel |
| python3-pwquality | git-instaweb | libssh2-docs |
| python3-pyudev | gitk | libsss_nss_idmap-devel |
| python3-pyverbs | git-subtree | libstemmer-devel |
| python3-pywbem | git-svn | libstoragemgmt-devel |
| python3-requests-oauthlib | gitweb | libsysfs-devel |
| python3-rtslib | gjs | libthai-devel |
| python3-samba | glade-libs | libtheora-devel |
| python3-samba-test | glassfish-annotation-api | libtiff-tools |
| python3-schedutils | glassfish-el | libucil-devel |
| python3-slip | glassfish-el-api | libudisks2-devel |
| python3-slip-dbus | glassfish-fastinfoset | libunicap-devel |
| python3-solv | glassfish-fastinfoset-javadoc | libuninameslist |
| python3-sss | glassfish-jaxb-api | libunistring-devel |
| python3-sssdconfig | glassfish-jaxb-api-javadoc | liburing-devel |
| python3-sss-murmur | glassfish-jaxb-codemodel | libusb-devel |
| python3-talloc | glassfish-jaxb-codemodel-annotation-compiler | libusbmuxd-devel |
| python3-tdb | glassfish-jaxb-core | libutempter-devel |
| python3-tevent | glassfish-jaxb-rngom | libuv-devel |
| python3-urwid | glassfish-jaxb-runtime | libv4l-devel |
| python3-varlink | glassfish-jaxb-txw2 | libvarlink-devel |
| quota | glassfish-jax-rs-api | libvdpau-devel |
| quota-doc | glassfish-jax-rs-api-javadoc | libvisio-devel |
| quota-nld | glassfish-jsp | libvisual-devel |
| quota-nls | glassfish-jsp-api | libvmem-debug |
| quota-rpc | glassfish-servlet-api | libvmmalloc-debug |
| quota-warnquota | glibc-utils | libvncserver-devel |
| rasdaemon | glibmm24 | libvoikko-devel |
| rdma-core-devel | gl-manpages | libvorbis-devel |
| readline-devel | glusterfs-api | libvorbis-devel-docs |
| readonly-root | glusterfs-cli | libvpx-devel |
| realmd | glx-utils | libwacom-devel |
| redhat-indexhtml | gnome-abrt | libwbclient-devel |
| redhat-release-eula | gnome-autoar | libwmf-devel |
| rhsm-icons | gnome-backgrounds | libwnck3-devel |
| rmt | gnome-backgrounds-extras | libwpd-devel |
| rng-tools | gnome-bluetooth | libwpd-doc |
| rpcbind | gnome-bluetooth-libs | libwpg-devel |
| rpm-apidocs | gnome-boxes | libwpg-doc |
| rpm-cron | gnome-calculator | libwps-devel |
| rpm-devel | gnome-characters | libwps-doc |
| rpm-plugin-ima | gnome-classic-session | libwsman-devel |
| rpm-plugin-prioreset | gnome-color-manager | libXdmcp-devel |
| rpm-plugin-syslog | gnome-control-center | libXfont2-devel |
| rpm-plugin-systemd-inhibit | gnome-control-center-filesystem | libxkbcommon-x11-devel |
| rpm-sign | gnome-desktop3 | libxkbfile-devel |
| rsync-daemon | gnome-desktop3-devel | libxklavier-devel |
| samba | gnome-disk-utility | libXNVCtrl-devel |
| samba-client | gnome-font-viewer | libXres-devel |
| samba-client-libs | gnome-getting-started-docs | libXvMC-devel |
| samba-common | gnome-getting-started-docs-cs | linuxdoc-tools |
| samba-common-libs | gnome-getting-started-docs-de | lockdev-devel |
| samba-common-tools | gnome-getting-started-docs-es | log4j12 |
| samba-dc-libs | gnome-getting-started-docs-fr | log4j12-javadoc |
| samba-krb5-printing | gnome-getting-started-docs-gl | log4j-over-slf4j |
| samba-libs | gnome-getting-started-docs-hu | lpsolve-devel |
| samba-pidl | gnome-getting-started-docs-it | lttng-ust-devel |
| samba-test | gnome-getting-started-docs-pl | lua-devel |
| samba-test-libs | gnome-getting-started-docs-pt_BR | lua-filesystem |
| samba-winbind | gnome-getting-started-docs-ru | lua-lunit |
| samba-winbind-clients | gnome-initial-setup | lua-posix |
| samba-winbind-krb5-locator | gnome-keyring | lvm2-devel |
| samba-winbind-modules | gnome-keyring-pam | lynx |
| samba-winexe | gnome-logs | maven |
| sanlock-lib | gnome-menus | maven2-javadoc |
| selinux-policy-devel | gnome-online-accounts | maven-antrun-plugin |
| selinux-policy-doc | gnome-online-accounts-devel | maven-antrun-plugin-javadoc |
| selinux-policy-mls | gnome-online-miners | maven-archiver |
| selinux-policy-sandbox | gnome-photos | maven-archiver-javadoc |
| setools-console | gnome-photos-tests | maven-artifact |
| setserial | gnome-remote-desktop | maven-artifact-manager |
| sgml-common | gnome-screenshot | maven-artifact-resolver |
| sgpio | gnome-session | maven-artifact-resolver-javadoc |
| shim-ia32 | gnome-session-kiosk-session | maven-artifact-transfer |
| shim-x64 | gnome-session-wayland-session | maven-artifact-transfer-javadoc |
| slang | gnome-session-xsession | maven-assembly-plugin |
| smartmontools | gnome-settings-daemon | maven-assembly-plugin-javadoc |
| smc-tools | gnome-shell | maven-cal10n-plugin |
| snappy | gnome-shell-extension-alternate-tab | maven-clean-plugin |
| sos | gnome-shell-extension-apps-menu | maven-clean-plugin-javadoc |
| sos-audit | gnome-shell-extension-auto-move-windows | maven-common-artifact-filters |
| sqlite-doc | gnome-shell-extension-common | maven-common-artifact-filters-javadoc |
| squashfs-tools | gnome-shell-extension-dash-to-dock | maven-compiler-plugin |
| srp_daemon | gnome-shell-extension-desktop-icons | maven-compiler-plugin-javadoc |
| sssd | gnome-shell-extension-disable-screenshield | maven-dependency-analyzer |
| sssd-ad | gnome-shell-extension-drive-menu | maven-dependency-analyzer-javadoc |
| sssd-client | gnome-shell-extension-gesture-inhibitor | maven-dependency-plugin |
| sssd-common | gnome-shell-extension-heads-up-display | maven-dependency-plugin-javadoc |
| sssd-common-pac | gnome-shell-extension-horizontal-workspaces | maven-dependency-tree |
| sssd-dbus | gnome-shell-extension-launch-new-instance | maven-dependency-tree-javadoc |
| sssd-ipa | gnome-shell-extension-native-window-placement | maven-doxia |
| sssd-kcm | gnome-shell-extension-no-hot-corner | maven-doxia-core |
| sssd-krb5 | gnome-shell-extension-panel-favorites | maven-doxia-javadoc |
| sssd-krb5-common | gnome-shell-extension-places-menu | maven-doxia-logging-api |
| sssd-ldap | gnome-shell-extension-screenshot-window-sizer | maven-doxia-module-apt |
| sssd-libwbclient | gnome-shell-extension-systemMonitor | maven-doxia-module-confluence |
| sssd-nfs-idmap | gnome-shell-extension-top-icons | maven-doxia-module-docbook-simple |
| sssd-polkit-rules | gnome-shell-extension-updates-dialog | maven-doxia-module-fml |
| sssd-proxy | gnome-shell-extension-user-theme | maven-doxia-module-latex |
| sssd-tools | gnome-shell-extension-window-grouper | maven-doxia-module-rtf |
| sssd-winbind-idmap | gnome-shell-extension-window-list | maven-doxia-modules |
| star | gnome-shell-extension-windowsNavigator | maven-doxia-module-twiki |
| strace | gnome-shell-extension-workspace-indicator | maven-doxia-module-xdoc |
| subscription-manager-cockpit | gnome-software | maven-doxia-module-xhtml |
| subscription-manager-plugin-container | gnome-software-editor | maven-doxia-sink-api |
| subscription-manager-plugin-ostree | gnome-system-monitor | maven-doxia-sitetools |
| symlinks | gnome-terminal | maven-doxia-sitetools-javadoc |
| syslinux | gnome-terminal-nautilus | maven-doxia-test-docs |
| syslinux-extlinux | gnome-themes-standard | maven-doxia-tests |
| syslinux-extlinux-nonlinux | gnome-tweaks | maven-enforcer |
| syslinux-nonlinux | gnome-user-docs | maven-enforcer-api |
| syslinux-tftpboot | gnome-video-effects | maven-enforcer-javadoc |
| systemd-container | gnu-free-fonts-common | maven-enforcer-plugin |
| systemd-journal-remote | gnu-free-mono-fonts | maven-enforcer-rules |
| systemd-tests | gnu-free-sans-fonts | maven-failsafe-plugin |
| system-storage-manager | gnu-free-serif-fonts | maven-file-management |
| target-restore | gnuplot | maven-file-management-javadoc |
| tboot | gnuplot-common | maven-filtering |
| tcl-devel | gnutls-c++ | maven-filtering-javadoc |
| tcl-doc | gnutls-devel | maven-hawtjni-plugin |
| tdb-tools | gobject-introspection-devel | maven-install-plugin |
| teamd | gom | maven-install-plugin-javadoc |
| timedatex | google-crosextra-caladea-fonts | maven-invoker |
| tmpwatch | google-crosextra-carlito-fonts | maven-invoker-javadoc |
| tmux | google-droid-kufi-fonts | maven-invoker-plugin |
| tpm2-abrmd | google-droid-sans-mono-fonts | maven-invoker-plugin-javadoc |
| tpm2-abrmd-selinux | google-droid-serif-fonts | maven-jar-plugin |
| tpm2-tools | google-gson | maven-jar-plugin-javadoc |
| tpm2-tss-devel | google-noto-cjk-fonts-common | maven-javadoc |
| tpm-quote-tools | google-noto-emoji-color-fonts | maven-lib |
| tpm-tools | google-noto-emoji-fonts | maven-local |
| tpm-tools-pkcs11 | google-noto-fonts-common | maven-model |
| trace-cmd | google-noto-kufi-arabic-fonts | maven-monitor |
| traceroute | google-noto-mono-fonts | maven-parent |
| tree | google-noto-naskh-arabic-fonts | maven-plugin-annotations |
| tss2 | google-noto-naskh-arabic-ui-fonts | maven-plugin-build-helper |
| tuna | google-noto-nastaliq-urdu-fonts | maven-plugin-build-helper-javadoc |
| tuned | google-noto-sans-armenian-fonts | maven-plugin-bundle |
| tuned-profiles-atomic | google-noto-sans-avestan-fonts | maven-plugin-bundle-javadoc |
| tuned-profiles-compat | google-noto-sans-balinese-fonts | maven-plugin-descriptor |
| tuned-profiles-cpu-partitioning | google-noto-sans-bamum-fonts | maven-plugin-plugin |
| tuned-profiles-mssql | google-noto-sans-batak-fonts | maven-plugin-registry |
| tuned-profiles-oracle | google-noto-sans-bengali-fonts | maven-plugins-pom |
| units | google-noto-sans-bengali-ui-fonts | maven-plugin-testing |
| usb_modeswitch | google-noto-sans-brahmi-fonts | maven-plugin-testing-harness |
| usb_modeswitch-data | google-noto-sans-buginese-fonts | maven-plugin-testing-javadoc |
| usbutils | google-noto-sans-buhid-fonts | maven-plugin-testing-tools |
| uuidd | google-noto-sans-canadian-aboriginal-fonts | maven-plugin-tools |
| vdo | google-noto-sans-carian-fonts | maven-plugin-tools-annotations |
| vdo-support | google-noto-sans-cham-fonts | maven-plugin-tools-ant |
| veritysetup | google-noto-sans-cherokee-fonts | maven-plugin-tools-api |
| vm-dump-metrics | google-noto-sans-cjk-ttc-fonts | maven-plugin-tools-beanshell |
| vm-dump-metrics-devel | google-noto-sans-coptic-fonts | maven-plugin-tools-generators |
| watchdog | google-noto-sans-cuneiform-fonts | maven-plugin-tools-java |
| words | google-noto-sans-cypriot-fonts | maven-plugin-tools-javadoc |
| wpa_supplicant | google-noto-sans-deseret-fonts | maven-plugin-tools-javadocs |
| x3270 | google-noto-sans-devanagari-fonts | maven-plugin-tools-model |
| x3270-text | google-noto-sans-devanagari-ui-fonts | maven-profile |
| xdelta | google-noto-sans-egyptian-hieroglyphs-fonts | maven-project |
| xfsdump | google-noto-sans-ethiopic-fonts | maven-remote-resources-plugin |
| xfsprogs | google-noto-sans-fonts | maven-remote-resources-plugin-javadoc |
| xfsprogs-devel | google-noto-sans-georgian-fonts | maven-reporting-api |
| xmlrpc-c | google-noto-sans-glagolitic-fonts | maven-reporting-api-javadoc |
| xmlrpc-c-client | google-noto-sans-gothic-fonts | maven-reporting-impl |
| zsh | google-noto-sans-gujarati-fonts | maven-reporting-impl-javadoc |
| google-noto-sans-gujarati-ui-fonts | maven-resolver |  |
| google-noto-sans-gurmukhi-fonts | maven-resolver-api |  |
| google-noto-sans-gurmukhi-ui-fonts | maven-resolver-connector-basic |  |
| google-noto-sans-hanunoo-fonts | maven-resolver-impl |  |
| google-noto-sans-hebrew-fonts | maven-resolver-javadoc |  |
| google-noto-sans-imperial-aramaic-fonts | maven-resolver-spi |  |
| google-noto-sans-inscriptional-pahlavi-fonts | maven-resolver-test-util |  |
| google-noto-sans-inscriptional-parthian-fonts | maven-resolver-transport-classpath |  |
| google-noto-sans-javanese-fonts | maven-resolver-transport-file |  |
| google-noto-sans-kaithi-fonts | maven-resolver-transport-http |  |
| google-noto-sans-kannada-fonts | maven-resolver-transport-wagon |  |
| google-noto-sans-kannada-ui-fonts | maven-resolver-util |  |
| google-noto-sans-kayah-li-fonts | maven-resources-plugin |  |
| google-noto-sans-kharoshthi-fonts | maven-resources-plugin-javadoc |  |
| google-noto-sans-khmer-fonts | maven-script |  |
| google-noto-sans-khmer-ui-fonts | maven-script-ant |  |
| google-noto-sans-lao-fonts | maven-script-beanshell |  |
| google-noto-sans-lao-ui-fonts | maven-script-interpreter |  |
| google-noto-sans-lepcha-fonts | maven-script-interpreter-javadoc |  |
| google-noto-sans-limbu-fonts | maven-settings |  |
| google-noto-sans-linear-b-fonts | maven-shade-plugin |  |
| google-noto-sans-lisu-fonts | maven-shade-plugin-javadoc |  |
| google-noto-sans-lycian-fonts | maven-shared |  |
| google-noto-sans-lydian-fonts | maven-shared-incremental |  |
| google-noto-sans-malayalam-fonts | maven-shared-incremental-javadoc |  |
| google-noto-sans-malayalam-ui-fonts | maven-shared-io |  |
| google-noto-sans-mandaic-fonts | maven-shared-io-javadoc |  |
| google-noto-sans-meetei-mayek-fonts | maven-shared-utils |  |
| google-noto-sans-mongolian-fonts | maven-shared-utils-javadoc |  |
| google-noto-sans-myanmar-fonts | maven-source-plugin |  |
| google-noto-sans-myanmar-ui-fonts | maven-source-plugin-javadoc |  |
| google-noto-sans-new-tai-lue-fonts | maven-surefire |  |
| google-noto-sans-nko-fonts | maven-surefire-javadoc |  |
| google-noto-sans-ogham-fonts | maven-surefire-plugin |  |
| google-noto-sans-ol-chiki-fonts | maven-surefire-provider-junit |  |
| google-noto-sans-old-italic-fonts | maven-surefire-provider-testng |  |
| google-noto-sans-old-persian-fonts | maven-surefire-report-parser |  |
| google-noto-sans-old-south-arabian-fonts | maven-surefire-report-plugin |  |
| google-noto-sans-old-turkic-fonts | maven-test-tools |  |
| google-noto-sans-oriya-fonts | maven-toolchain |  |
| google-noto-sans-oriya-ui-fonts | maven-verifier |  |
| google-noto-sans-osmanya-fonts | maven-verifier-javadoc |  |
| google-noto-sans-phags-pa-fonts | maven-wagon |  |
| google-noto-sans-phoenician-fonts | maven-wagon-file |  |
| google-noto-sans-rejang-fonts | maven-wagon-ftp |  |
| google-noto-sans-runic-fonts | maven-wagon-http |  |
| google-noto-sans-samaritan-fonts | maven-wagon-http-lightweight |  |
| google-noto-sans-saurashtra-fonts | maven-wagon-http-shared |  |
| google-noto-sans-shavian-fonts | maven-wagon-javadoc |  |
| google-noto-sans-sinhala-fonts | maven-wagon-provider-api |  |
| google-noto-sans-sundanese-fonts | maven-wagon-providers |  |
| google-noto-sans-syloti-nagri-fonts | memkind-devel |  |
| google-noto-sans-symbols-fonts | mesa-libgbm-devel |  |
| google-noto-sans-syriac-eastern-fonts | mesa-libGLES-devel |  |
| google-noto-sans-syriac-estrangela-fonts | mesa-libOSMesa-devel |  |
| google-noto-sans-syriac-western-fonts | meson |  |
| google-noto-sans-tagalog-fonts | metis |  |
| google-noto-sans-tagbanwa-fonts | metis-devel |  |
| google-noto-sans-tai-le-fonts | mingw32-binutils |  |
| google-noto-sans-tai-tham-fonts | mingw32-bzip2 |  |
| google-noto-sans-tai-viet-fonts | mingw32-bzip2-static |  |
| google-noto-sans-tamil-fonts | mingw32-cairo |  |
| google-noto-sans-tamil-ui-fonts | mingw32-cpp |  |
| google-noto-sans-telugu-fonts | mingw32-crt |  |
| google-noto-sans-telugu-ui-fonts | mingw32-expat |  |
| google-noto-sans-thaana-fonts | mingw32-filesystem |  |
| google-noto-sans-thai-fonts | mingw32-fontconfig |  |
| google-noto-sans-thai-ui-fonts | mingw32-freetype |  |
| google-noto-sans-tibetan-fonts | mingw32-freetype-static |  |
| google-noto-sans-tifinagh-fonts | mingw32-gcc |  |
| google-noto-sans-ugaritic-fonts | mingw32-gcc-c++ |  |
| google-noto-sans-ui-fonts | mingw32-gettext |  |
| google-noto-sans-vai-fonts | mingw32-gettext-static |  |
| google-noto-sans-yi-fonts | mingw32-glib2 |  |
| google-noto-serif-armenian-fonts | mingw32-glib2-static |  |
| google-noto-serif-bengali-fonts | mingw32-gstreamer1 |  |
| google-noto-serif-cjk-ttc-fonts | mingw32-harfbuzz |  |
| google-noto-serif-devanagari-fonts | mingw32-harfbuzz-static |  |
| google-noto-serif-fonts | mingw32-headers |  |
| google-noto-serif-georgian-fonts | mingw32-icu |  |
| google-noto-serif-gujarati-fonts | mingw32-libffi |  |
| google-noto-serif-kannada-fonts | mingw32-libjpeg-turbo |  |
| google-noto-serif-khmer-fonts | mingw32-libjpeg-turbo-static |  |
| google-noto-serif-lao-fonts | mingw32-libpng |  |
| google-noto-serif-malayalam-fonts | mingw32-libpng-static |  |
| google-noto-serif-tamil-fonts | mingw32-libtiff |  |
| google-noto-serif-telugu-fonts | mingw32-libtiff-static |  |
| google-noto-serif-thai-fonts | mingw32-openssl |  |
| gpm | mingw32-pcre |  |
| gpm-devel | mingw32-pcre-static |  |
| grafana | mingw32-pixman |  |
| grafana-azure-monitor | mingw32-pkg-config |  |
| grafana-cloudwatch | mingw32-readline |  |
| grafana-elasticsearch | mingw32-spice-vdagent |  |
| grafana-graphite | mingw32-sqlite |  |
| grafana-influxdb | mingw32-sqlite-static |  |
| grafana-loki | mingw32-termcap |  |
| grafana-mssql | mingw32-win-iconv |  |
| grafana-mysql | mingw32-win-iconv-static |  |
| grafana-opentsdb | mingw32-winpthreads |  |
| grafana-pcp | mingw32-winpthreads-static |  |
| grafana-postgres | mingw32-zlib |  |
| grafana-prometheus | mingw32-zlib-static |  |
| grafana-stackdriver | mingw64-binutils |  |
| graphite2-devel | mingw64-bzip2 |  |
| graphviz | mingw64-bzip2-static |  |
| greenboot | mingw64-cairo |  |
| greenboot-grub2 | mingw64-cpp |  |
| greenboot-reboot | mingw64-crt |  |
| greenboot-rpm-ostree-grub2 | mingw64-expat |  |
| greenboot-status | mingw64-filesystem |  |
| grilo | mingw64-fontconfig |  |
| grilo-plugins | mingw64-freetype |  |
| gsettings-desktop-schemas-devel | mingw64-freetype-static |  |
| gsl | mingw64-gcc |  |
| gsl-devel | mingw64-gcc-c++ |  |
| gsound | mingw64-gettext |  |
| gspell | mingw64-gettext-static |  |
| gssdp | mingw64-glib2 |  |
| gssntlmssp | mingw64-glib2-static |  |
| gstreamer1 | mingw64-gstreamer1 |  |
| gstreamer1-devel | mingw64-harfbuzz |  |
| gstreamer1-plugins-bad-free | mingw64-harfbuzz-static |  |
| gstreamer1-plugins-base | mingw64-headers |  |
| gstreamer1-plugins-base-devel | mingw64-icu |  |
| gstreamer1-plugins-good | mingw64-libffi |  |
| gstreamer1-plugins-good-gtk | mingw64-libjpeg-turbo |  |
| gstreamer1-plugins-ugly-free | mingw64-libjpeg-turbo-static |  |
| gtk2-devel | mingw64-libpng |  |
| gtk2-devel-docs | mingw64-libpng-static |  |
| gtk2-immodules | mingw64-libtiff |  |
| gtk2-immodule-xim | mingw64-libtiff-static |  |
| gtk3-devel | mingw64-openssl |  |
| gtk3-immodule-xim | mingw64-pcre |  |
| gtkmm24 | mingw64-pcre-static |  |
| gtkmm30 | mingw64-pixman |  |
| gtksourceview3 | mingw64-pkg-config |  |
| gtkspell | mingw64-readline |  |
| gtkspell3 | mingw64-spice-vdagent |  |
| gtk-vnc2 | mingw64-sqlite |  |
| gubbi-fonts | mingw64-sqlite-static |  |
| gupnp | mingw64-termcap |  |
| gupnp-av | mingw64-win-iconv |  |
| gupnp-dlna | mingw64-win-iconv-static |  |
| gupnp-igd | mingw64-winpthreads |  |
| gutenprint | mingw64-winpthreads-static |  |
| gutenprint-cups | mingw64-zlib |  |
| gutenprint-doc | mingw64-zlib-static |  |
| gutenprint-libs | mingw-binutils-generic |  |
| gutenprint-libs-ui | mingw-filesystem-base |  |
| gutenprint-plugin | mobile-broadband-provider-info-devel |  |
| gvfs | mockito |  |
| gvfs-afc | mockito-javadoc |  |
| gvfs-afp | modello |  |
| gvfs-archive | modello-javadoc |  |
| gvfs-client | ModemManager-devel |  |
| gvfs-devel | ModemManager-glib-devel |  |
| gvfs-fuse | mojo-parent |  |
| gvfs-goa | mozjs52-devel |  |
| gvfs-gphoto2 | mozjs60-devel |  |
| gvfs-mtp | mpg123-devel |  |
| gvfs-smb | msv-msv |  |
| gvnc | mtdev-devel |  |
| hamcrest | munge-devel |  |
| hamcrest-core | munge-maven-plugin |  |
| harfbuzz-devel | munge-maven-plugin-javadoc |  |
| harfbuzz-icu | mutter-devel |  |
| HdrHistogram | mythes-devel |  |
| HdrHistogram_c | nasm |  |
| HdrHistogram-javadoc | nautilus-devel |  |
| hesiod | ndctl-devel |  |
| hexchat | neon-devel |  |
| hexchat-devel | netpbm-devel |  |
| hexedit | netpbm-doc |  |
| highlight | NetworkManager-libnm-devel |  |
| highlight-gui | nghttp2 |  |
| hivex | ninja-build |  |
| hivex-devel | nkf |  |
| hplip | nss_hesiod |  |
| hplip-common | objectweb-asm |  |
| hplip-gui | objectweb-asm-javadoc |  |
| hplip-libs | objectweb-pom |  |
| hspell | objenesis |  |
| http-parser | objenesis-javadoc |  |
| hunspell-af | ocaml |  |
| hunspell-ak | ocaml-camlp4 |  |
| hunspell-am | ocaml-camlp4-devel |  |
| hunspell-ar | ocaml-compiler-libs |  |
| hunspell-as | ocaml-cppo |  |
| hunspell-ast | ocaml-extlib |  |
| hunspell-az | ocaml-extlib-devel |  |
| hunspell-be | ocaml-findlib |  |
| hunspell-ber | ocaml-findlib-devel |  |
| hunspell-bg | ocaml-hivex |  |
| hunspell-bn | ocaml-hivex-devel |  |
| hunspell-br | ocaml-labltk |  |
| hunspell-ca | ocaml-labltk-devel |  |
| hunspell-cop | ocaml-libguestfs |  |
| hunspell-cs | ocaml-libguestfs-devel |  |
| hunspell-csb | ocaml-libnbd |  |
| hunspell-cv | ocaml-libnbd-devel |  |
| hunspell-cy | ocaml-ocamlbuild |  |
| hunspell-da | ocaml-ocamlbuild-devel |  |
| hunspell-de | ocaml-ocamldoc |  |
| hunspell-devel | ocaml-runtime |  |
| hunspell-dsb | ocl-icd-devel |  |
| hunspell-el | oniguruma-devel |  |
| hunspell-eo | openal-soft-devel |  |
| hunspell-es | openblas-devel |  |
| hunspell-es-AR | openblas-openmp |  |
| hunspell-es-BO | openblas-openmp64 |  |
| hunspell-es-CL | openblas-openmp64_ |  |
| hunspell-es-CO | openblas-Rblas |  |
| hunspell-es-CR | openblas-serial64 |  |
| hunspell-es-CU | openblas-serial64_ |  |
| hunspell-es-DO | openblas-static |  |
| hunspell-es-EC | openblas-threads64 |  |
| hunspell-es-ES | openblas-threads64_ |  |
| hunspell-es-GT | opencl-headers |  |
| hunspell-es-HN | opencryptoki-devel |  |
| hunspell-es-MX | opencv |  |
| hunspell-es-NI | opencv-devel |  |
| hunspell-es-PA | OpenEXR-devel |  |
| hunspell-es-PE | OpenIPMI-devel |  |
| hunspell-es-PR | openjade |  |
| hunspell-es-PY | openjpeg2-devel |  |
| hunspell-es-SV | openldap-servers |  |
| hunspell-es-US | openscap-engine-sce-devel |  |
| hunspell-es-UY | opensm-devel |  |
| hunspell-es-VE | opensp |  |
| hunspell-et | opensp-devel |  |
| hunspell-eu | opus-devel |  |
| hunspell-fa | osgi-annotation |  |
| hunspell-fj | osgi-annotation-javadoc |  |
| hunspell-fo | osgi-compendium |  |
| hunspell-fr | osgi-compendium-javadoc |  |
| hunspell-fur | osgi-core |  |
| hunspell-fy | osgi-core-javadoc |  |
| hunspell-ga | os-maven-plugin |  |
| hunspell-gd | os-maven-plugin-javadoc |  |
| hunspell-gl | PackageKit-glib-devel |  |
| hunspell-grc | pandoc |  |
| hunspell-gu | pandoc-common |  |
| hunspell-gv | pangomm-devel |  |
| hunspell-haw | pangomm-doc |  |
| hunspell-he | papi-testsuite |  |
| hunspell-hi | parted-devel |  |
| hunspell-hil | pcre2-tools |  |
| hunspell-hr | pcre-static |  |
| hunspell-hsb | pcsc-lite-devel |  |
| hunspell-ht | perl-AnyEvent |  |
| hunspell-hu | perl-B-Hooks-EndOfScope |  |
| hunspell-hy | perl-Canary-Stability |  |
| hunspell-ia | perl-Capture-Tiny |  |
| hunspell-id | perl-Class-Accessor |  |
| hunspell-is | perl-Class-Data-Inheritable |  |
| hunspell-it | perl-Class-Factory-Util |  |
| hunspell-kk | perl-Class-Method-Modifiers |  |
| hunspell-km | perl-Class-Singleton |  |
| hunspell-kn | perl-Class-Tiny |  |
| hunspell-ko | perl-Class-XSAccessor |  |
| hunspell-ku | perl-Clone |  |
| hunspell-ky | perl-common-sense |  |
| hunspell-la | perl-Config-AutoConf |  |
| hunspell-lb | perl-Data-UUID |  |
| hunspell-ln | perl-Date-ISO8601 |  |
| hunspell-lt | perl-DateTime |  |
| hunspell-lv | perl-DateTime-Format-Builder |  |
| hunspell-mai | perl-DateTime-Format-HTTP |  |
| hunspell-mg | perl-DateTime-Format-ISO8601 |  |
| hunspell-mi | perl-DateTime-Format-Mail |  |
| hunspell-mk | perl-DateTime-Format-Strptime |  |
| hunspell-ml | perl-DateTime-Locale |  |
| hunspell-mn | perl-DateTime-TimeZone |  |
| hunspell-mos | perl-DateTime-TimeZone-SystemV |  |
| hunspell-mr | perl-DateTime-TimeZone-Tzfile |  |
| hunspell-ms | perl-Devel-CallChecker |  |
| hunspell-mt | perl-Devel-Caller |  |
| hunspell-nb | perl-Devel-CheckLib |  |
| hunspell-nds | perl-Devel-GlobalDestruction |  |
| hunspell-ne | perl-Devel-LexAlias |  |
| hunspell-nl | perl-Devel-StackTrace |  |
| hunspell-nn | perl-Devel-Symdump |  |
| hunspell-nr | perl-Digest-CRC |  |
| hunspell-nso | perl-Digest-SHA1 |  |
| hunspell-ny | perl-Dist-CheckConflicts |  |
| hunspell-oc | perl-DynaLoader-Functions |  |
| hunspell-om | perl-Eval-Closure |  |
| hunspell-or | perl-Exception-Class |  |
| hunspell-pa | perl-Exporter-Tiny |  |
| hunspell-pl | perl-File-BaseDir |  |
| hunspell-pt | perl-File-chdir |  |
| hunspell-qu | perl-File-Copy-Recursive |  |
| hunspell-quh | perl-File-DesktopEntry |  |
| hunspell-ro | perl-File-Find-Object |  |
| hunspell-ru | perl-File-Find-Rule |  |
| hunspell-rw | perl-File-MimeInfo |  |
| hunspell-sc | perl-File-ReadBackwards |  |
| hunspell-se | perl-File-Remove |  |
| hunspell-shs | perl-HTML-Tree |  |
| hunspell-si | perl-HTTP-Daemon |  |
| hunspell-sk | perl-Importer |  |
| hunspell-sl | perl-Import-Into |  |
| hunspell-smj | perl-IO-All |  |
| hunspell-so | perl-IO-String |  |
| hunspell-sq | perl-IO-stringy |  |
| hunspell-sr | perl-IO-Tty |  |
| hunspell-ss | perl-IPC-Run |  |
| hunspell-st | perl-IPC-Run3 |  |
| hunspell-sv | perl-JSON-XS |  |
| hunspell-sw | perl-List-MoreUtils |  |
| hunspell-ta | perl-List-MoreUtils-XS |  |
| hunspell-te | perl-Locale-gettext |  |
| hunspell-tet | perl-MIME-Charset |  |
| hunspell-th | perl-MIME-Types |  |
| hunspell-ti | perl-Module-Implementation |  |
| hunspell-tk | perl-Module-Install |  |
| hunspell-tl | perl-Module-Install-AuthorTests |  |
| hunspell-tn | perl-Module-Install-ReadmeFromPod |  |
| hunspell-tpi | perl-Module-ScanDeps |  |
| hunspell-ts | perl-namespace-autoclean |  |
| hunspell-uk | perl-namespace-clean |  |
| hunspell-ur | perl-NKF |  |
| hunspell-uz | perl-Number-Compare |  |
| hunspell-ve | perl-Package-DeprecationManager |  |
| hunspell-vi | perl-Package-Stash |  |
| hunspell-wa | perl-Package-Stash-XS |  |
| hunspell-xh | perl-PadWalker |  |
| hunspell-yi | perl-Params-Classify |  |
| hunspell-zu | perl-Params-Validate |  |
| hwloc-gui | perl-Params-ValidationCompiler |  |
| hwloc-plugins | perl-Path-Tiny |  |
| hyperv-daemons | perl-Perl-Destruct-Level |  |
| hyperv-daemons-license | perl-PerlIO-utf8_strict |  |
| hypervfcopyd | perl-Pod-Coverage |  |
| hypervkvpd | perl-Pod-Markdown |  |
| hyperv-tools | perl-prefork |  |
| hypervvssd | perl-Readonly |  |
| hyphen | perl-Ref-Util |  |
| hyphen-af | perl-Ref-Util-XS |  |
| hyphen-as | perl-Role-Tiny |  |
| hyphen-be | perl-Scope-Guard |  |
| hyphen-bg | perl-SGMLSpm |  |
| hyphen-bn | perl-Specio |  |
| hyphen-ca | perl-Sub-Exporter-Progressive |  |
| hyphen-cs | perl-Sub-Identify |  |
| hyphen-cy | perl-Sub-Info |  |
| hyphen-da | perl-Sub-Name |  |
| hyphen-de | perl-Sub-Uplevel |  |
| hyphen-el | perl-SUPER |  |
| hyphen-en | perl-Switch |  |
| hyphen-es | perl-Taint-Runtime |  |
| hyphen-et | perl-Term-Size-Any |  |
| hyphen-eu | perl-Term-Size-Perl |  |
| hyphen-fa | perl-Term-Table |  |
| hyphen-fo | perl-Test2-Suite |  |
| hyphen-fr | perl-Test-Deep |  |
| hyphen-ga | perl-Test-Differences |  |
| hyphen-gl | perl-Test-Exception |  |
| hyphen-grc | perl-Test-Fatal |  |
| hyphen-gu | perl-Test-LongString |  |
| hyphen-hi | perl-Test-NoWarnings |  |
| hyphen-hr | perl-Test-Pod |  |
| hyphen-hsb | perl-Test-Pod-Coverage |  |
| hyphen-hu | perl-Test-Requires |  |
| hyphen-ia | perl-Test-Taint |  |
| hyphen-id | perl-Test-Warn |  |
| hyphen-is | perl-Test-Warnings |  |
| hyphen-it | perl-Text-CharWidth |  |
| hyphen-kn | perl-Text-WrapI18N |  |
| hyphen-ku | perltidy |  |
| hyphen-lt | perl-Tie-IxHash |  |
| hyphen-lv | perl-Tk |  |
| hyphen-mi | perl-Tk-devel |  |
| hyphen-ml | perl-Types-Serialiser |  |
| hyphen-mn | perl-Unicode-EastAsianWidth |  |
| hyphen-mr | perl-Unicode-LineBreak |  |
| hyphen-nb | perl-Unicode-UTF8 |  |
| hyphen-nl | perl-Variable-Magic |  |
| hyphen-nn | perl-XML-DOM |  |
| hyphen-or | perl-XML-RegExp |  |
| hyphen-pa | perl-XML-Twig |  |
| hyphen-pl | perl-YAML-LibYAML |  |
| hyphen-pt | perl-YAML-Syck |  |
| hyphen-ro | perl-YAML-Tiny |  |
| hyphen-ru | pidgin-devel |  |
| hyphen-sa | plexus-ant-factory |  |
| hyphen-sk | plexus-ant-factory-javadoc |  |
| hyphen-sl | plexus-archiver |  |
| hyphen-sr | plexus-archiver-javadoc |  |
| hyphen-sv | plexus-bsh-factory |  |
| hyphen-ta | plexus-bsh-factory-javadoc |  |
| hyphen-te | plexus-build-api |  |
| hyphen-tk | plexus-build-api-javadoc |  |
| hyphen-uk | plexus-cipher |  |
| hyphen-zu | plexus-cipher-javadoc |  |
| i2c-tools | plexus-classworlds |  |
| i2c-tools-perl | plexus-classworlds-javadoc |  |
| ibus | plexus-cli |  |
| ibus-gtk2 | plexus-cli-javadoc |  |
| ibus-gtk3 | plexus-compiler |  |
| ibus-hangul | plexus-compiler-extras |  |
| ibus-kkc | plexus-compiler-javadoc |  |
| ibus-libpinyin | plexus-compiler-pom |  |
| ibus-libs | plexus-component-api |  |
| ibus-libzhuyin | plexus-component-api-javadoc |  |
| ibus-m17n | plexus-component-factories-pom |  |
| ibus-sayura | plexus-components-pom |  |
| ibus-setup | plexus-containers |  |
| ibus-table | plexus-containers-component-annotations |  |
| ibus-table-chinese | plexus-containers-component-javadoc |  |
| ibus-table-chinese-array | plexus-containers-component-metadata |  |
| ibus-table-chinese-cangjie | plexus-containers-container-default |  |
| ibus-table-chinese-cantonese | plexus-containers-javadoc |  |
| ibus-table-chinese-easy | plexus-i18n |  |
| ibus-table-chinese-erbi | plexus-i18n-javadoc |  |
| ibus-table-chinese-quick | plexus-interactivity |  |
| ibus-table-chinese-scj | plexus-interactivity-api |  |
| ibus-table-chinese-stroke5 | plexus-interactivity-javadoc |  |
| ibus-table-chinese-wu | plexus-interactivity-jline |  |
| ibus-table-chinese-wubi-haifeng | plexus-interpolation |  |
| ibus-table-chinese-wubi-jidian | plexus-interpolation-javadoc |  |
| ibus-table-chinese-yong | plexus-io |  |
| ibus-typing-booster | plexus-io-javadoc |  |
| ibus-wayland | plexus-languages |  |
| icedax | plexus-languages-javadoc |  |
| icedtea-web | plexus-pom |  |
| icedtea-web-javadoc | plexus-resources |  |
| icoutils | plexus-resources-javadoc |  |
| icu4j | plexus-sec-dispatcher |  |
| idn2 | plexus-sec-dispatcher-javadoc |  |
| iio-sensor-proxy | plexus-utils |  |
| initial-setup | plexus-utils-javadoc |  |
| initial-setup-gui | plexus-velocity |  |
| inkscape | plexus-velocity-javadoc |  |
| inkscape-docs | plotutils |  |
| inkscape-view | plotutils-devel |  |
| insights-client | pmix-devel |  |
| intel-gpu-tools | po4a |  |
| intltool | poppler-cpp |  |
| iowatcher | poppler-cpp-devel |  |
| ipa-client | poppler-devel |  |
| ipa-client-common | poppler-glib-devel |  |
| ipa-client-epn | poppler-qt5 |  |
| ipa-client-samba | poppler-qt5-devel |  |
| ipa-common | powermock-api-easymock |  |
| ipa-healthcheck | powermock-api-mockito |  |
| ipa-healthcheck-core | powermock-api-support |  |
| ipa-idoverride-memberof-plugin | powermock-common |  |
| ipa-python-compat | powermock-core |  |
| ipa-selinux | powermock-javadoc |  |
| ipa-server | powermock-junit4 |  |
| ipa-server-common | powermock-reflect |  |
| ipa-server-dns | powermock-testng |  |
| ipa-server-trust-ad | ppp-devel |  |
| iperf3 | pps-tools-devel |  |
| ipmievd | protobuf-c-compiler |  |
| ipmitool | protobuf-c-devel |  |
| ipvsadm | protobuf-compiler |  |
| ipxe-bootimgs | protobuf-devel |  |
| ipxe-roms | protobuf-lite-devel |  |
| ipxe-roms-qemu | pstoedit |  |
| irssi | ptscotch-mpich |  |
| iso-codes-devel | ptscotch-mpich-devel |  |
| isomd5sum | ptscotch-mpich-devel-parmetis |  |
| istack-commons-runtime | ptscotch-openmpi |  |
| istack-commons-tools | ptscotch-openmpi-devel |  |
| itstool | py3c-devel |  |
| jackson-annotations | py3c-doc |  |
| jackson-annotations-javadoc | pygobject3-devel |  |
| jackson-core | python2-gluster |  |
| jackson-core-javadoc | python2-iso8601 |  |
| jackson-databind | python2-talloc |  |
| jackson-databind-javadoc | python38-atomicwrites |  |
| jackson-jaxrs-json-provider | python38-attrs |  |
| jackson-jaxrs-providers | python38-more-itertools |  |
| jackson-jaxrs-providers-datatypes | python38-packaging |  |
| jackson-jaxrs-providers-javadoc | python38-pluggy |  |
| jackson-module-jaxb-annotations | python38-py |  |
| jackson-module-jaxb-annotations-javadoc | python38-pyparsing |  |
| jaf | python38-pytest |  |
| jaf-javadoc | python38-wcwidth |  |
| jakarta-commons-httpclient | python39-attrs |  |
| jakarta-commons-httpclient-demo | python39-Cython |  |
| jakarta-commons-httpclient-javadoc | python39-debug |  |
| jakarta-commons-httpclient-manual | python39-iniconfig |  |
| jansson-devel | python39-more-itertools |  |
| java-11-openjdk-demo | python39-packaging |  |
| java-11-openjdk-javadoc | python39-pluggy |  |
| java-11-openjdk-javadoc-zip | python39-py |  |
| java-11-openjdk-jmods | python39-pybind11 |  |
| java-11-openjdk-src | python39-pybind11-devel |  |
| java-11-openjdk-static-libs | python39-pyparsing |  |
| java-17-openjdk-demo | python39-pytest |  |
| java-17-openjdk-javadoc | python39-wcwidth |  |
| java-17-openjdk-javadoc-zip | python3-Cython |  |
| java-17-openjdk-jmods | python3-greenlet-devel |  |
| java-17-openjdk-src | python3-httplib2 |  |
| java-17-openjdk-static-libs | python3-hypothesis |  |
| java-1.8.0-openjdk-accessibility | python3-imagesize |  |
| java-1.8.0-openjdk-demo | python3-iso8601 |  |
| java-1.8.0-openjdk-javadoc | python3-javapackages |  |
| java-1.8.0-openjdk-javadoc-zip | python3-lesscpy |  |
| java-1.8.0-openjdk-src | python3-libpfm |  |
| java-atk-wrapper | python3-markdown |  |
| javassist | python3-mock |  |
| javassist-javadoc | python3-mpich |  |
| jboss-annotations-1.2-api | python3-openmpi |  |
| jboss-jaxrs-2.0-api | python3-packaging |  |
| jboss-logging | python3-pillow-devel |  |
| jboss-logging-tools | python3-pillow-doc |  |
| jdeparser | python3-pillow-tk |  |
| jetty-continuation | python3-pyxattr |  |
| jetty-http | python3-qt5-devel |  |
| jetty-io | python3-rrdtool |  |
| jetty-security | python3-scons |  |
| jetty-server | python3-setuptools_scm |  |
| jetty-servlet | python3-sip-devel |  |
| jetty-util | python3-snowballstemmer |  |
| jigawatts | python3-sphinx |  |
| jigawatts-javadoc | python3-sphinxcontrib-websupport |  |
| jline | python3-sphinx_rtd_theme |  |
| jmc | python3-sphinx-theme-alabaster |  |
| jmc-core | python3-sure |  |
| jmc-core-javadoc | python3-unittest2 |  |
| jna | python3-whoosh |  |
| jomolhari-fonts | python-cups-doc |  |
| jose | python-sphinx-locale |  |
| jsch | qatlib-devel |  |
| js-d3-flame-graph | qdox |  |
| json-c-devel | qdox-javadoc |  |
| json-glib-devel | qemu-kvm-tests |  |
| jss | qgpgme-devel |  |
| jss-javadoc | qhull-devel |  |
| Judy | qrencode-devel |  |
| julietaula-montserrat-fonts | qt5-devel |  |
| junit | qt5-qtbase-static |  |
| junit5 | qt5-qtdeclarative-static |  |
| jzlib | qt5-qtquickcontrols2-devel |  |
| kacst-art-fonts | qt5-qtserialbus-devel |  |
| kacst-book-fonts | qt5-qttools-static |  |
| kacst-decorative-fonts | qt5-qtwayland-devel |  |
| kacst-digital-fonts | quota-devel |  |
| kacst-farsi-fonts | raptor2-devel |  |
| kacst-fonts-common | rasqal-devel |  |
| kacst-letter-fonts | re2c |  |
| kacst-naskh-fonts | recode-devel |  |
| kacst-office-fonts | redland-devel |  |
| kacst-one-fonts | regexp |  |
| kacst-pen-fonts | regexp-javadoc |  |
| kacst-poster-fonts | rpcgen |  |
| kacst-qurn-fonts | rpcsvc-proto-devel |  |
| kacst-screen-fonts | rrdtool-devel |  |
| kacst-title-fonts | rrdtool-doc |  |
| kacst-titlel-fonts | rrdtool-lua |  |
| kdump-anaconda-addon | rrdtool-ruby |  |
| kernel-rpm-macros | rrdtool-tcl |  |
| kernelshark | rubygem-diff-lcs |  |
| keybinder3 | rubygem-rspec |  |
| keycloak-httpd-client-install | rubygem-rspec-core |  |
| khmeros-base-fonts | rubygem-rspec-expectations |  |
| khmeros-battambang-fonts | rubygem-rspec-mocks |  |
| khmeros-bokor-fonts | rubygem-rspec-support |  |
| khmeros-fonts-common | samba-devel |  |
| khmeros-handwritten-fonts | sanlock-devel |  |
| khmeros-metal-chrieng-fonts | sblim-cmpi-devel |  |
| khmeros-muol-fonts | sblim-gather-provider |  |
| khmeros-siemreap-fonts | sblim-sfcc-devel |  |
| koan | scotch |  |
| ksh | scotch-devel |  |
| kurdit-unikurd-web-fonts | SDL2 |  |
| kyotocabinet-libs | SDL2-devel |  |
| lame-libs | SDL2-static |  |
| langpacks-af | sendmail-milter-devel |  |
| langpacks-am | sg3_utils-devel |  |
| langpacks-ar | sharutils |  |
| langpacks-as | shim-unsigned-x64 |  |
| langpacks-ast | sip |  |
| langpacks-be | sisu-inject |  |
| langpacks-bg | sisu-javadoc |  |
| langpacks-bn | sisu-mojos |  |
| langpacks-br | sisu-mojos-javadoc |  |
| langpacks-bs | sisu-plexus |  |
| langpacks-ca | slf4j |  |
| langpacks-cs | slf4j-ext |  |
| langpacks-cy | slf4j-javadoc |  |
| langpacks-da | slf4j-jcl |  |
| langpacks-de | slf4j-jdk14 |  |
| langpacks-el | slf4j-log4j12 |  |
| langpacks-en_GB | slf4j-manual |  |
| langpacks-es | slf4j-sources |  |
| langpacks-et | snappy-devel |  |
| langpacks-eu | socket_wrapper |  |
| langpacks-fa | sombok |  |
| langpacks-fi | sombok-devel |  |
| langpacks-fr | sonatype-oss-parent |  |
| langpacks-ga | sonatype-plugins-parent |  |
| langpacks-gl | soundtouch-devel |  |
| langpacks-gu | sparsehash-devel |  |
| langpacks-he | spec-version-maven-plugin |  |
| langpacks-hi | spec-version-maven-plugin-javadoc |  |
| langpacks-hr | speech-dispatcher-devel |  |
| langpacks-hu | speech-dispatcher-doc |  |
| langpacks-ia | speex-devel |  |
| langpacks-id | speexdsp-devel |  |
| langpacks-is | spice-parent |  |
| langpacks-it | spice-server-devel |  |
| langpacks-ja | spirv-tools-devel |  |
| langpacks-kk | spirv-tools-libs |  |
| langpacks-kn | suitesparse-devel |  |
| langpacks-ko | SuperLU |  |
| langpacks-lt | SuperLU-devel |  |
| langpacks-lv | taglib-devel |  |
| langpacks-mai | tesseract-devel |  |
| langpacks-mk | testng |  |
| langpacks-ml | testng-javadoc |  |
| langpacks-mr | texi2html |  |
| langpacks-ms | texinfo |  |
| langpacks-nb | texinfo-tex |  |
| langpacks-ne | texlive-lib-devel |  |
| langpacks-nl | tinycdb |  |
| langpacks-nn | tinycdb-devel |  |
| langpacks-nr | tinyxml2 |  |
| langpacks-nso | tinyxml2-devel |  |
| langpacks-or | tix-devel |  |
| langpacks-pa | tog-pegasus-devel |  |
| langpacks-pl | tokyocabinet-devel |  |
| langpacks-pt | torque |  |
| langpacks-pt_BR | torque-devel |  |
| langpacks-ro | tpm2-abrmd-devel |  |
| langpacks-ru | tpm-tools-devel |  |
| langpacks-si | tracker-devel |  |
| langpacks-sk | transfig |  |
| langpacks-sl | trousers-devel |  |
| langpacks-sq | tss2-devel |  |
| langpacks-sr | turbojpeg-devel |  |
| langpacks-ss | twolame-devel |  |
| langpacks-sv | uglify-js |  |
| langpacks-ta | uid_wrapper |  |
| langpacks-te | unicode-ucd-unihan |  |
| langpacks-th | upower-devel |  |
| langpacks-tn | upower-devel-docs |  |
| langpacks-tr | urw-base35-fonts-devel |  |
| langpacks-ts | usbredir-devel |  |
| langpacks-uk | userspace-rcu-devel |  |
| langpacks-ur | ustr |  |
| langpacks-ve | uthash-devel |  |
| langpacks-vi | uuid-devel |  |
| langpacks-xh | vala |  |
| langpacks-zh_CN | vala-devel |  |
| langpacks-zh_TW | velocity |  |
| langpacks-zu | velocity-demo |  |
| langtable | velocity-javadoc |  |
| langtable-data | velocity-manual |  |
| lapack | vte291-devel |  |
| lapack64 | wavpack-devel |  |
| lato-fonts | web-assets-devel |  |
| ldapjdk | web-assets-filesystem |  |
| ldapjdk-javadoc | weld-parent |  |
| ldns | wireshark-devel |  |
| lemon | woff2-devel |  |
| leptonica | xalan-j2 |  |
| lftp | xalan-j2-demo |  |
| lftp-scripts | xalan-j2-javadoc |  |
| liba52 | xalan-j2-manual |  |
| libabw | xalan-j2-xsltc |  |
| libadwaita-qt5 | xapian-core-devel |  |
| libao | Xaw3d-devel |  |
| libappindicator-gtk3 | xbean |  |
| libasan6 | xbean-javadoc |  |
| libatasmart | xcb-proto |  |
| libavc1394 | xcb-util-devel |  |
| libbase | xcb-util-image-devel |  |
| libblockdev | xcb-util-keysyms-devel |  |
| libblockdev-crypto | xcb-util-renderutil-devel |  |
| libblockdev-dm | xcb-util-wm-devel |  |
| libblockdev-fs | xerces-j2 |  |
| libblockdev-kbd | xerces-j2-demo |  |
| libblockdev-loop | xerces-j2-javadoc |  |
| libblockdev-lvm | xhtml1-dtds |  |
| libblockdev-lvm-dbus | xkeyboard-config-devel |  |
| libblockdev-mdraid | xml-commons-apis |  |
| libblockdev-mpath | xml-commons-apis-javadoc |  |
| libblockdev-nvdimm | xml-commons-apis-manual |  |
| libblockdev-part | xml-commons-resolver |  |
| libblockdev-plugins-all | xml-commons-resolver-javadoc |  |
| libblockdev-swap | xmlrpc-c-c++ |  |
| libblockdev-utils | xmlrpc-c-client++ |  |
| libblockdev-vdo | xmlrpc-c-devel |  |
| libbluray | xmlsec1-devel |  |
| libbpf-devel | xmlsec1-gcrypt |  |
| libburn | xmlsec1-gnutls |  |
| libbytesize | xmlsec1-gnutls-devel |  |
| libcacard | xmlsec1-openssl-devel |  |
| libcacard-devel | xmltoman |  |
| libcanberra | xmlunit |  |
| libcanberra-devel | xmlunit-javadoc |  |
| libcanberra-gtk2 | xmvn |  |
| libcanberra-gtk3 | xmvn-api |  |
| libcdio | xmvn-bisect |  |
| libcdio-paranoia | xmvn-connector-aether |  |
| libcdr | xmvn-connector-ivy |  |
| libcmis | xmvn-core |  |
| libcmpiCppImpl0 | xmvn-install |  |
| libdazzle | xmvn-javadoc |  |
| libdbusmenu | xmvn-minimal |  |
| libdbusmenu-gtk3 | xmvn-mojo |  |
| libdc1394 | xmvn-parent-pom |  |
| libdmapsharing | xmvn-resolve |  |
| libdmx | xmvn-subst |  |
| libdnet | xmvn-tools-pom |  |
| libdrm-devel | xorg-x11-apps |  |
| libdv | xorg-x11-drv-libinput-devel |  |
| libdvdnav | xorg-x11-drv-wacom-devel |  |
| libdvdread | xorg-x11-server-devel |  |
| libdwarf | xorg-x11-server-source |  |
| libeasyfc | xorg-x11-util-macros |  |
| libeasyfc-gobject | xorg-x11-xkb-utils-devel |  |
| libecap | xorg-x11-xtrans-devel |  |
| libecap-devel | xz-java |  |
| libecpg | xz-java-javadoc |  |
| libepoxy-devel | xz-lzma-compat |  |
| libepubgen | yajl-devel |  |
| libetonyek | yasm |  |
| libev | yelp-devel |  |
| libevdev | zlib-static |  |
| libev-devel | zziplib-devel |  |
| libev-libevent-devel |  |  |
| libev-source |  |  |
| libexttextcat |  |  |
| libfonts |  |  |
| libformula |  |  |
| libfprint |  |  |
| libfreehand |  |  |
| libgdata |  |  |
| libgdata-devel |  |  |
| libgdither |  |  |
| libgee |  |  |
| libgexiv2 |  |  |
| libgit2 |  |  |
| libgit2-glib |  |  |
| libglvnd-core-devel |  |  |
| libglvnd-devel |  |  |
| libglvnd-gles |  |  |
| libglvnd-opengl |  |  |
| libgnomekbd |  |  |
| libgovirt |  |  |
| libgphoto2 |  |  |
| libgpod |  |  |
| libgsf |  |  |
| libgtop2 |  |  |
| libguestfs |  |  |
| libguestfs-bash-completion |  |  |
| libguestfs-benchmarking |  |  |
| libguestfs-devel |  |  |
| libguestfs-gfs2 |  |  |
| libguestfs-gobject |  |  |
| libguestfs-gobject-devel |  |  |
| libguestfs-inspect-icons |  |  |
| libguestfs-java |  |  |
| libguestfs-java-devel |  |  |
| libguestfs-javadoc |  |  |
| libguestfs-man-pages-ja |  |  |
| libguestfs-man-pages-uk |  |  |
| libguestfs-rescue |  |  |
| libguestfs-rsync |  |  |
| libguestfs-tools |  |  |
| libguestfs-tools-c |  |  |
| libguestfs-winsupport |  |  |
| libguestfs-xfs |  |  |
| libgweather |  |  |
| libgweather-devel |  |  |
| libgxps |  |  |
| libhangul |  |  |
| libi2c |  |  |
| libical-devel |  |  |
| libICE-devel |  |  |
| libidn2-devel |  |  |
| libiec61883 |  |  |
| libieee1284 |  |  |
| libieee1284-devel |  |  |
| libimobiledevice |  |  |
| libindicator-gtk3 |  |  |
| libinput |  |  |
| libinput-utils |  |  |
| libiptcdata |  |  |
| libiscsi |  |  |
| libiscsi-devel |  |  |
| libiscsi-utils |  |  |
| libisoburn |  |  |
| libisofs |  |  |
| libitm-devel |  |  |
| libjose |  |  |
| libjose-devel |  |  |
| libjpeg-turbo-utils |  |  |
| libkkc |  |  |
| libkkc-common |  |  |
| libkkc-data |  |  |
| liblangtag |  |  |
| liblangtag-data |  |  |
| liblayout |  |  |
| libloader |  |  |
| liblockfile |  |  |
| liblognorm |  |  |
| liblognorm-doc |  |  |
| liblouis |  |  |
| libluksmeta |  |  |
| libluksmeta-devel |  |  |
| libmad |  |  |
| libmalaga |  |  |
| libmatchbox |  |  |
| libmaxminddb-devel |  |  |
| libmediaart |  |  |
| libmng |  |  |
| libmng-devel |  |  |
| libmnl-devel |  |  |
| libmpcdec |  |  |
| libmpc-devel |  |  |
| libmspack |  |  |
| libmspub |  |  |
| libmtp |  |  |
| libmusicbrainz5 |  |  |
| libmwaw |  |  |
| libnbd |  |  |
| libnbd-devel |  |  |
| libnice |  |  |
| libnice-gstreamer1 |  |  |
| libnma |  |  |
| libnotify |  |  |
| libnotify-devel |  |  |
| libnumbertext |  |  |
| liboauth |  |  |
| liboauth-devel |  |  |
| libodfgen |  |  |
| libogg-devel |  |  |
| libopenraw |  |  |
| liborcus |  |  |
| libosinfo |  |  |
| libotf |  |  |
| libpagemaker |  |  |
| libpciaccess-devel |  |  |
| libpeas-gtk |  |  |
| libpeas-loader-python3 |  |  |
| libpfm |  |  |
| libpfm-devel |  |  |
| libpgtypes |  |  |
| libpinyin |  |  |
| libpinyin-data |  |  |
| libplist |  |  |
| libpmem |  |  |
| libpmemblk |  |  |
| libpmemblk-debug |  |  |
| libpmemblk-devel |  |  |
| libpmem-debug |  |  |
| libpmem-devel |  |  |
| libpmemlog |  |  |
| libpmemlog-debug |  |  |
| libpmemlog-devel |  |  |
| libpmemobj |  |  |
| libpmemobj-debug |  |  |
| libpmemobj-devel |  |  |
| libpmemobj++-devel |  |  |
| libpmemobj++-doc |  |  |
| libpmempool |  |  |
| libpmempool-debug |  |  |
| libpmempool-devel |  |  |
| libpng12 |  |  |
| libpng15 |  |  |
| libproxy-bin |  |  |
| libproxy-gnome |  |  |
| libproxy-networkmanager |  |  |
| libproxy-webkitgtk4 |  |  |
| libpst-libs |  |  |
| libpurple |  |  |
| libquvi |  |  |
| libquvi-scripts |  |  |
| libqxp |  |  |
| librados2 |  |  |
| LibRaw |  |  |
| libraw1394 |  |  |
| librbd1 |  |  |
| librdkafka |  |  |
| librelp |  |  |
| libreoffice-base |  |  |
| libreoffice-calc |  |  |
| libreoffice-core |  |  |
| libreoffice-data |  |  |
| libreoffice-draw |  |  |
| libreoffice-emailmerge |  |  |
| libreoffice-filters |  |  |
| libreoffice-gdb-debug-support |  |  |
| libreoffice-graphicfilter |  |  |
| libreoffice-gtk2 |  |  |
| libreoffice-gtk3 |  |  |
| libreoffice-help-ar |  |  |
| libreoffice-help-bg |  |  |
| libreoffice-help-bn |  |  |
| libreoffice-help-ca |  |  |
| libreoffice-help-cs |  |  |
| libreoffice-help-da |  |  |
| libreoffice-help-de |  |  |
| libreoffice-help-dz |  |  |
| libreoffice-help-el |  |  |
| libreoffice-help-en |  |  |
| libreoffice-help-es |  |  |
| libreoffice-help-et |  |  |
| libreoffice-help-eu |  |  |
| libreoffice-help-fi |  |  |
| libreoffice-help-fr |  |  |
| libreoffice-help-gl |  |  |
| libreoffice-help-gu |  |  |
| libreoffice-help-he |  |  |
| libreoffice-help-hi |  |  |
| libreoffice-help-hr |  |  |
| libreoffice-help-hu |  |  |
| libreoffice-help-id |  |  |
| libreoffice-help-it |  |  |
| libreoffice-help-ja |  |  |
| libreoffice-help-ko |  |  |
| libreoffice-help-lt |  |  |
| libreoffice-help-lv |  |  |
| libreoffice-help-nb |  |  |
| libreoffice-help-nl |  |  |
| libreoffice-help-nn |  |  |
| libreoffice-help-pl |  |  |
| libreoffice-help-pt-BR |  |  |
| libreoffice-help-pt-PT |  |  |
| libreoffice-help-ro |  |  |
| libreoffice-help-ru |  |  |
| libreoffice-help-si |  |  |
| libreoffice-help-sk |  |  |
| libreoffice-help-sl |  |  |
| libreoffice-help-sv |  |  |
| libreoffice-help-ta |  |  |
| libreoffice-help-tr |  |  |
| libreoffice-help-uk |  |  |
| libreoffice-help-zh-Hans |  |  |
| libreoffice-help-zh-Hant |  |  |
| libreoffice-impress |  |  |
| libreofficekit |  |  |
| libreoffice-langpack-af |  |  |
| libreoffice-langpack-ar |  |  |
| libreoffice-langpack-as |  |  |
| libreoffice-langpack-bg |  |  |
| libreoffice-langpack-bn |  |  |
| libreoffice-langpack-br |  |  |
| libreoffice-langpack-ca |  |  |
| libreoffice-langpack-cs |  |  |
| libreoffice-langpack-cy |  |  |
| libreoffice-langpack-da |  |  |
| libreoffice-langpack-de |  |  |
| libreoffice-langpack-dz |  |  |
| libreoffice-langpack-el |  |  |
| libreoffice-langpack-en |  |  |
| libreoffice-langpack-es |  |  |
| libreoffice-langpack-et |  |  |
| libreoffice-langpack-eu |  |  |
| libreoffice-langpack-fa |  |  |
| libreoffice-langpack-fi |  |  |
| libreoffice-langpack-fr |  |  |
| libreoffice-langpack-ga |  |  |
| libreoffice-langpack-gl |  |  |
| libreoffice-langpack-gu |  |  |
| libreoffice-langpack-he |  |  |
| libreoffice-langpack-hi |  |  |
| libreoffice-langpack-hr |  |  |
| libreoffice-langpack-hu |  |  |
| libreoffice-langpack-id |  |  |
| libreoffice-langpack-it |  |  |
| libreoffice-langpack-ja |  |  |
| libreoffice-langpack-kk |  |  |
| libreoffice-langpack-kn |  |  |
| libreoffice-langpack-ko |  |  |
| libreoffice-langpack-lt |  |  |
| libreoffice-langpack-lv |  |  |
| libreoffice-langpack-mai |  |  |
| libreoffice-langpack-ml |  |  |
| libreoffice-langpack-mr |  |  |
| libreoffice-langpack-nb |  |  |
| libreoffice-langpack-nl |  |  |
| libreoffice-langpack-nn |  |  |
| libreoffice-langpack-nr |  |  |
| libreoffice-langpack-nso |  |  |
| libreoffice-langpack-or |  |  |
| libreoffice-langpack-pa |  |  |
| libreoffice-langpack-pl |  |  |
| libreoffice-langpack-pt-BR |  |  |
| libreoffice-langpack-pt-PT |  |  |
| libreoffice-langpack-ro |  |  |
| libreoffice-langpack-ru |  |  |
| libreoffice-langpack-si |  |  |
| libreoffice-langpack-sk |  |  |
| libreoffice-langpack-sl |  |  |
| libreoffice-langpack-sr |  |  |
| libreoffice-langpack-ss |  |  |
| libreoffice-langpack-st |  |  |
| libreoffice-langpack-sv |  |  |
| libreoffice-langpack-ta |  |  |
| libreoffice-langpack-te |  |  |
| libreoffice-langpack-th |  |  |
| libreoffice-langpack-tn |  |  |
| libreoffice-langpack-tr |  |  |
| libreoffice-langpack-ts |  |  |
| libreoffice-langpack-uk |  |  |
| libreoffice-langpack-ve |  |  |
| libreoffice-langpack-xh |  |  |
| libreoffice-langpack-zh-Hans |  |  |
| libreoffice-langpack-zh-Hant |  |  |
| libreoffice-langpack-zu |  |  |
| libreoffice-math |  |  |
| libreoffice-ogltrans |  |  |
| libreoffice-opensymbol-fonts |  |  |
| libreoffice-pdfimport |  |  |
| libreoffice-pyuno |  |  |
| libreoffice-ure |  |  |
| libreoffice-ure-common |  |  |
| libreoffice-voikko |  |  |
| libreoffice-wiki-publisher |  |  |
| libreoffice-writer |  |  |
| libreoffice-x11 |  |  |
| libreoffice-xsltfilter |  |  |
| libreport |  |  |
| libreport-anaconda |  |  |
| libreport-cli |  |  |
| libreport-gtk |  |  |
| libreport-newt |  |  |
| libreport-plugin-bugzilla |  |  |
| libreport-plugin-kerneloops |  |  |
| libreport-plugin-logger |  |  |
| libreport-plugin-mailx |  |  |
| libreport-plugin-reportuploader |  |  |
| libreport-plugin-rhtsupport |  |  |
| libreport-plugin-ureport |  |  |
| libreport-rhel |  |  |
| libreport-rhel-anaconda-bugzilla |  |  |
| libreport-rhel-bugzilla |  |  |
| libreport-web |  |  |
| librepository |  |  |
| libreswan |  |  |
| librevenge |  |  |
| librevenge-gdb |  |  |
| librpmem |  |  |
| librpmem-debug |  |  |
| librpmem-devel |  |  |
| librsvg2-devel |  |  |
| librsvg2-tools |  |  |
| libsamplerate |  |  |
| libsane-hpaio |  |  |
| libselinux-python |  |  |
| libserf |  |  |
| libserializer |  |  |
| libshout |  |  |
| libsigc++20 |  |  |
| libSM-devel |  |  |
| libsmi |  |  |
| libsoup-devel |  |  |
| libspectre |  |  |
| libspiro |  |  |
| libsrtp |  |  |
| libssh2 |  |  |
| libssh-devel |  |  |
| libstaroffice |  |  |
| libstdc++-docs |  |  |
| libtar |  |  |
| libtasn1-devel |  |  |
| libtasn1-tools |  |  |
| libtheora |  |  |
| libtimezonemap |  |  |
| libtool-ltdl-devel |  |  |
| libucil |  |  |
| libudisks2 |  |  |
| libunicap |  |  |
| liburing |  |  |
| libusal |  |  |
| libusbmuxd |  |  |
| libv4l |  |  |
| libva |  |  |
| libva-devel |  |  |
| libvdpau |  |  |
| libvirt |  |  |
| libvirt-admin |  |  |
| libvirt-bash-completion |  |  |
| libvirt-client |  |  |
| libvirt-daemon |  |  |
| libvirt-daemon-config-network |  |  |
| libvirt-daemon-config-nwfilter |  |  |
| libvirt-daemon-driver-interface |  |  |
| libvirt-daemon-driver-network |  |  |
| libvirt-daemon-driver-nodedev |  |  |
| libvirt-daemon-driver-nwfilter |  |  |
| libvirt-daemon-driver-qemu |  |  |
| libvirt-daemon-driver-secret |  |  |
| libvirt-daemon-driver-storage |  |  |
| libvirt-daemon-driver-storage-core |  |  |
| libvirt-daemon-driver-storage-disk |  |  |
| libvirt-daemon-driver-storage-gluster |  |  |
| libvirt-daemon-driver-storage-iscsi |  |  |
| libvirt-daemon-driver-storage-iscsi-direct |  |  |
| libvirt-daemon-driver-storage-logical |  |  |
| libvirt-daemon-driver-storage-mpath |  |  |
| libvirt-daemon-driver-storage-rbd |  |  |
| libvirt-daemon-driver-storage-scsi |  |  |
| libvirt-daemon-kvm |  |  |
| libvirt-dbus |  |  |
| libvirt-devel |  |  |
| libvirt-docs |  |  |
| libvirt-gconfig |  |  |
| libvirt-glib |  |  |
| libvirt-gobject |  |  |
| libvirt-libs |  |  |
| libvirt-lock-sanlock |  |  |
| libvirt-nss |  |  |
| libvisio |  |  |
| libvisual |  |  |
| libvma |  |  |
| libvmem |  |  |
| libvmem-devel |  |  |
| libvmmalloc |  |  |
| libvmmalloc-devel |  |  |
| libvncserver |  |  |
| libvoikko |  |  |
| libvpx |  |  |
| libwacom |  |  |
| libwacom-data |  |  |
| libwinpr |  |  |
| libwinpr-devel |  |  |
| libwmf |  |  |
| libwnck3 |  |  |
| libwpd |  |  |
| libwpg |  |  |
| libwps |  |  |
| libwsman1 |  |  |
| libXaw |  |  |
| libXaw-devel |  |  |
| libxcam |  |  |
| libXcomposite-devel |  |  |
| libXcursor-devel |  |  |
| libXdamage-devel |  |  |
| libXdmcp |  |  |
| libxdp |  |  |
| libXext-devel |  |  |
| libXfixes-devel |  |  |
| libXfont2 |  |  |
| libXft-devel |  |  |
| libXi-devel |  |  |
| libXinerama-devel |  |  |
| libxkbcommon-devel |  |  |
| libxkbcommon-x11 |  |  |
| libxkbfile |  |  |
| libxklavier |  |  |
| libXmu-devel |  |  |
| libXNVCtrl |  |  |
| libXp |  |  |
| libXp-devel |  |  |
| libXrandr-devel |  |  |
| libXrender-devel |  |  |
| libXres |  |  |
| libXScrnSaver-devel |  |  |
| libxshmfence-devel |  |  |
| libXt-devel |  |  |
| libXtst-devel |  |  |
| libXv |  |  |
| libXv-devel |  |  |
| libXvMC |  |  |
| libXxf86dga |  |  |
| libXxf86dga-devel |  |  |
| libXxf86misc-devel |  |  |
| libXxf86vm-devel |  |  |
| libyami |  |  |
| libyang |  |  |
| libyang-devel |  |  |
| libzhuyin |  |  |
| libzmf |  |  |
| linuxconsoletools |  |  |
| linuxptp |  |  |
| lklug-fonts |  |  |
| lldpd |  |  |
| lldpd-devel |  |  |
| lmdb-libs |  |  |
| lm_sensors-sensord |  |  |
| log4j12 |  |  |
| log4j12-javadoc |  |  |
| lohit-assamese-fonts |  |  |
| lohit-bengali-fonts |  |  |
| lohit-devanagari-fonts |  |  |
| lohit-gujarati-fonts |  |  |
| lohit-gurmukhi-fonts |  |  |
| lohit-kannada-fonts |  |  |
| lohit-malayalam-fonts |  |  |
| lohit-marathi-fonts |  |  |
| lohit-nepali-fonts |  |  |
| lohit-odia-fonts |  |  |
| lohit-tamil-fonts |  |  |
| lohit-telugu-fonts |  |  |
| lorax |  |  |
| lorax-composer |  |  |
| lorax-lmc-novirt |  |  |
| lorax-lmc-virt |  |  |
| lorax-templates-generic |  |  |
| lorax-templates-rhel |  |  |
| lpsolve |  |  |
| lshw-gui |  |  |
| ltrace |  |  |
| lua-expat |  |  |
| lua-guestfs |  |  |
| lua-json |  |  |
| lua-lpeg |  |  |
| lua-socket |  |  |
| lucene |  |  |
| lucene-analysis |  |  |
| lucene-analyzers-smartcn |  |  |
| lucene-queries |  |  |
| lucene-queryparser |  |  |
| lucene-sandbox |  |  |
| luksmeta |  |  |
| lz4-java |  |  |
| lz4-java-javadoc |  |  |
| m17n-db |  |  |
| m17n-lib |  |  |
| madan-fonts |  |  |
| mailman |  |  |
| malaga |  |  |
| malaga-suomi-voikko |  |  |
| mallard-rng |  |  |
| man-pages-overrides |  |  |
| mariadb |  |  |
| mariadb-backup |  |  |
| mariadb-common |  |  |
| mariadb-connector-odbc |  |  |
| mariadb-devel |  |  |
| mariadb-embedded |  |  |
| mariadb-embedded-devel |  |  |
| mariadb-errmsg |  |  |
| mariadb-gssapi-server |  |  |
| mariadb-java-client |  |  |
| mariadb-oqgraph-engine |  |  |
| mariadb-pam |  |  |
| mariadb-server |  |  |
| mariadb-server-galera |  |  |
| mariadb-server-utils |  |  |
| mariadb-test |  |  |
| marisa |  |  |
| matchbox-window-manager |  |  |
| mc |  |  |
| mdevctl |  |  |
| meanwhile |  |  |
| mecab |  |  |
| mecab-ipadic |  |  |
| mecab-ipadic-EUCJP |  |  |
| media-player-info |  |  |
| memkind |  |  |
| mercurial |  |  |
| mercurial-hgk |  |  |
| mesa-dri-drivers |  |  |
| mesa-filesystem |  |  |
| mesa-khr-devel |  |  |
| mesa-libEGL-devel |  |  |
| mesa-libGL-devel |  |  |
| mesa-libGLES |  |  |
| mesa-libGLU |  |  |
| mesa-libGLU-devel |  |  |
| mesa-libGLw |  |  |
| mesa-libGLw-devel |  |  |
| mesa-libOSMesa |  |  |
| mesa-libxatracker |  |  |
| mesa-vdpau-drivers |  |  |
| mesa-vulkan-devel |  |  |
| mesa-vulkan-drivers |  |  |
| metacity |  |  |
| micropipenv |  |  |
| mod_auth_mellon-diagnostics |  |  |
| mod_auth_openidc |  |  |
| mod_dav_svn |  |  |
| modulemd-tools |  |  |
| motif |  |  |
| motif-devel |  |  |
| motif-static |  |  |
| mousetweaks |  |  |
| mozilla-filesystem |  |  |
| mozvoikko |  |  |
| mpfr-devel |  |  |
| mpg123 |  |  |
| mpg123-libs |  |  |
| mpg123-plugins-pulseaudio |  |  |
| mpich |  |  |
| mpich-devel |  |  |
| mpich-doc |  |  |
| mpitests-mpich |  |  |
| mpitests-mvapich2 |  |  |
| mpitests-mvapich2-psm2 |  |  |
| mpitests-openmpi |  |  |
| mrtg |  |  |
| mstflint |  |  |
| msv-javadoc |  |  |
| msv-manual |  |  |
| msv-xsdlib |  |  |
| mtdev |  |  |
| mtr-gtk |  |  |
| mt-st |  |  |
| mtx |  |  |
| multilib-rpm-config |  |  |
| munge |  |  |
| munge-libs |  |  |
| mutt |  |  |
| mutter |  |  |
| mvapich2 |  |  |
| mvapich2-devel |  |  |
| mvapich2-doc |  |  |
| mvapich2-psm2 |  |  |
| mvapich2-psm2-devel |  |  |
| mysql |  |  |
| mysql-common |  |  |
| mysql-devel |  |  |
| mysql-errmsg |  |  |
| mysql-libs |  |  |
| mysql-selinux |  |  |
| mysql-server |  |  |
| mysql-test |  |  |
| mythes |  |  |
| mythes-bg |  |  |
| mythes-ca |  |  |
| mythes-cs |  |  |
| mythes-da |  |  |
| mythes-de |  |  |
| mythes-el |  |  |
| mythes-en |  |  |
| mythes-es |  |  |
| mythes-fr |  |  |
| mythes-ga |  |  |
| mythes-hu |  |  |
| mythes-it |  |  |
| mythes-lb |  |  |
| mythes-lv |  |  |
| mythes-mi |  |  |
| mythes-nb |  |  |
| mythes-ne |  |  |
| mythes-nl |  |  |
| mythes-nn |  |  |
| mythes-pl |  |  |
| mythes-pt |  |  |
| mythes-ro |  |  |
| mythes-ru |  |  |
| mythes-sk |  |  |
| mythes-sl |  |  |
| mythes-sv |  |  |
| mythes-uk |  |  |
| nafees-web-naskh-fonts |  |  |
| nautilus |  |  |
| nautilus-extensions |  |  |
| nautilus-sendto |  |  |
| navilu-fonts |  |  |
| nbdfuse |  |  |
| nbdkit |  |  |
| nbdkit-bash-completion |  |  |
| nbdkit-basic-filters |  |  |
| nbdkit-basic-plugins |  |  |
| nbdkit-curl-plugin |  |  |
| nbdkit-devel |  |  |
| nbdkit-example-plugins |  |  |
| nbdkit-gzip-plugin |  |  |
| nbdkit-linuxdisk-plugin |  |  |
| nbdkit-plugin-gzip |  |  |
| nbdkit-plugin-python3 |  |  |
| nbdkit-plugin-python-common |  |  |
| nbdkit-plugin-vddk |  |  |
| nbdkit-plugin-xz |  |  |
| nbdkit-python-plugin |  |  |
| nbdkit-server |  |  |
| nbdkit-ssh-plugin |  |  |
| nbdkit-vddk-plugin |  |  |
| nbdkit-xz-filter |  |  |
| ncompress |  |  |
| ndctl-devel |  |  |
| neon |  |  |
| netcf |  |  |
| netcf-devel |  |  |
| netcf-libs |  |  |
| netpbm |  |  |
| netpbm-progs |  |  |
| net-snmp-devel |  |  |
| net-snmp-perl |  |  |
| net-snmp-utils |  |  |
| nettle-devel |  |  |
| network-manager-applet |  |  |
| NetworkManager-cloud-setup |  |  |
| NetworkManager-libreswan |  |  |
| NetworkManager-libreswan-gnome |  |  |
| newt-devel |  |  |
| nispor |  |  |
| nispor-devel |  |  |
| nm-connection-editor |  |  |
| nmstate |  |  |
| nmstate-plugin-ovsdb |  |  |
| nspr-devel |  |  |
| nss-altfiles |  |  |
| nss-devel |  |  |
| nss-pam-ldapd |  |  |
| nss-softokn-devel |  |  |
| nss-softokn-freebl-devel |  |  |
| nss-util-devel |  |  |
| ntpstat |  |  |
| objectweb-asm |  |  |
| oci-systemd-hook |  |  |
| oci-umount |  |  |
| ocl-icd |  |  |
| oddjob |  |  |
| oddjob-mkhomedir |  |  |
| omping |  |  |
| ongres-scram |  |  |
| ongres-scram-client |  |  |
| openal-soft |  |  |
| openchange |  |  |
| opencl-filesystem |  |  |
| opencv |  |  |
| opencv-contrib |  |  |
| opencv-core |  |  |
| opendnssec |  |  |
| openjpeg2-devel-docs |  |  |
| openjpeg2-tools |  |  |
| openmpi |  |  |
| openmpi-devel |  |  |
| open-sans-fonts |  |  |
| openscap-devel |  |  |
| openscap-engine-sce |  |  |
| openscap-python3 |  |  |
| openscap-utils |  |  |
| openslp |  |  |
| openssh-askpass |  |  |
| opentest4j |  |  |
| open-vm-tools |  |  |
| open-vm-tools-desktop |  |  |
| open-vm-tools-sdmp |  |  |
| openwsman-client |  |  |
| openwsman-python3 |  |  |
| openwsman-server |  |  |
| opus |  |  |
| opus-devel |  |  |
| orc |  |  |
| orca |  |  |
| orc-compiler |  |  |
| orc-devel |  |  |
| osad |  |  |
| osbuild |  |  |
| osbuild-composer |  |  |
| osbuild-composer-core |  |  |
| osbuild-composer-worker |  |  |
| osbuild-ostree |  |  |
| osbuild-selinux |  |  |
| oscap-anaconda-addon |  |  |
| osinfo-db |  |  |
| osinfo-db-tools |  |  |
| ostree |  |  |
| ostree-grub2 |  |  |
| overpass-fonts |  |  |
| overpass-mono-fonts |  |  |
| owasp-java-encoder |  |  |
| owasp-java-encoder-javadoc |  |  |
| pacemaker-cluster-libs |  |  |
| pacemaker-libs |  |  |
| pacemaker-schemas |  |  |
| PackageKit |  |  |
| PackageKit-command-not-found |  |  |
| PackageKit-cron |  |  |
| PackageKit-glib |  |  |
| PackageKit-gstreamer-plugin |  |  |
| PackageKit-gtk3-module |  |  |
| pakchois |  |  |
| paktype-naqsh-fonts |  |  |
| paktype-naskh-basic-fonts |  |  |
| paktype-tehreer-fonts |  |  |
| pango-devel |  |  |
| pangomm |  |  |
| papi |  |  |
| papi-devel |  |  |
| papi-libs |  |  |
| paps |  |  |
| paps-libs |  |  |
| paratype-pt-sans-caption-fonts |  |  |
| paratype-pt-sans-fonts |  |  |
| parfait |  |  |
| parfait-examples |  |  |
| parfait-javadoc |  |  |
| patchutils |  |  |
| pavucontrol |  |  |
| pcaudiolib |  |  |
| pcm |  |  |
| pcp |  |  |
| pcp-conf |  |  |
| pcp-devel |  |  |
| pcp-doc |  |  |
| pcp-export-pcp2elasticsearch |  |  |
| pcp-export-pcp2graphite |  |  |
| pcp-export-pcp2influxdb |  |  |
| pcp-export-pcp2json |  |  |
| pcp-export-pcp2spark |  |  |
| pcp-export-pcp2xml |  |  |
| pcp-export-pcp2zabbix |  |  |
| pcp-export-zabbix-agent |  |  |
| pcp-gui |  |  |
| pcp-import-collectl2pcp |  |  |
| pcp-import-ganglia2pcp |  |  |
| pcp-import-iostat2pcp |  |  |
| pcp-import-mrtg2pcp |  |  |
| pcp-import-sar2pcp |  |  |
| pcp-libs |  |  |
| pcp-libs-devel |  |  |
| pcp-manager |  |  |
| pcp-parfait-agent |  |  |
| pcp-pmda-activemq |  |  |
| pcp-pmda-apache |  |  |
| pcp-pmda-bash |  |  |
| pcp-pmda-bcc |  |  |
| pcp-pmda-bind2 |  |  |
| pcp-pmda-bonding |  |  |
| pcp-pmda-bpftrace |  |  |
| pcp-pmda-cifs |  |  |
| pcp-pmda-cisco |  |  |
| pcp-pmda-dbping |  |  |
| pcp-pmda-dm |  |  |
| pcp-pmda-docker |  |  |
| pcp-pmda-ds389 |  |  |
| pcp-pmda-ds389log |  |  |
| pcp-pmda-elasticsearch |  |  |
| pcp-pmda-gfs2 |  |  |
| pcp-pmda-gluster |  |  |
| pcp-pmda-gpfs |  |  |
| pcp-pmda-gpsd |  |  |
| pcp-pmda-hacluster |  |  |
| pcp-pmda-haproxy |  |  |
| pcp-pmda-infiniband |  |  |
| pcp-pmda-json |  |  |
| pcp-pmda-libvirt |  |  |
| pcp-pmda-lio |  |  |
| pcp-pmda-lmsensors |  |  |
| pcp-pmda-logger |  |  |
| pcp-pmda-lustre |  |  |
| pcp-pmda-lustrecomm |  |  |
| pcp-pmda-mailq |  |  |
| pcp-pmda-memcache |  |  |
| pcp-pmda-mic |  |  |
| pcp-pmda-mounts |  |  |
| pcp-pmda-mssql |  |  |
| pcp-pmda-mysql |  |  |
| pcp-pmda-named |  |  |
| pcp-pmda-netcheck |  |  |
| pcp-pmda-netfilter |  |  |
| pcp-pmda-news |  |  |
| pcp-pmda-nfsclient |  |  |
| pcp-pmda-nginx |  |  |
| pcp-pmda-nvidia-gpu |  |  |
| pcp-pmda-openmetrics |  |  |
| pcp-pmda-openvswitch |  |  |
| pcp-pmda-oracle |  |  |
| pcp-pmda-papi |  |  |
| pcp-pmda-pdns |  |  |
| pcp-pmda-perfevent |  |  |
| pcp-pmda-podman |  |  |
| pcp-pmda-postfix |  |  |
| pcp-pmda-postgresql |  |  |
| pcp-pmda-prometheus |  |  |
| pcp-pmda-rabbitmq |  |  |
| pcp-pmda-redis |  |  |
| pcp-pmda-roomtemp |  |  |
| pcp-pmda-rpm |  |  |
| pcp-pmda-rsyslog |  |  |
| pcp-pmda-samba |  |  |
| pcp-pmda-sendmail |  |  |
| pcp-pmda-shping |  |  |
| pcp-pmda-slurm |  |  |
| pcp-pmda-smart |  |  |
| pcp-pmda-snmp |  |  |
| pcp-pmda-sockets |  |  |
| pcp-pmda-statsd |  |  |
| pcp-pmda-summary |  |  |
| pcp-pmda-systemd |  |  |
| pcp-pmda-trace |  |  |
| pcp-pmda-unbound |  |  |
| pcp-pmda-vmware |  |  |
| pcp-pmda-weblog |  |  |
| pcp-pmda-zimbra |  |  |
| pcp-pmda-zswap |  |  |
| pcp-selinux |  |  |
| pcp-system-tools |  |  |
| pcp-testsuite |  |  |
| pcp-webapi |  |  |
| pcp-webapp-blinkenlights |  |  |
| pcp-webapp-grafana |  |  |
| pcp-webapp-graphite |  |  |
| pcp-webapp-vector |  |  |
| pcp-webjs |  |  |
| pcp-zeroconf |  |  |
| pentaho-libxml |  |  |
| pentaho-reporting-flow-engine |  |  |
| peripety |  |  |
| perl-Authen-SASL |  |  |
| perl-Bit-Vector |  |  |
| perl-B-Lint |  |  |
| perl-Carp-Clan |  |  |
| perl-CGI |  |  |
| perl-Class-Inspector |  |  |
| perl-Class-ISA |  |  |
| perl-Convert-ASN1 |  |  |
| perl-core |  |  |
| perl-Crypt-OpenSSL-Bignum |  |  |
| perl-Crypt-OpenSSL-Random |  |  |
| perl-Crypt-OpenSSL-RSA |  |  |
| perl-Date-Calc |  |  |
| perl-DBD-MySQL |  |  |
| perl-DBD-Pg |  |  |
| perl-DBD-SQLite |  |  |
| perl-DBI |  |  |
| perl-Encode-Detect |  |  |
| perl-File-CheckTree |  |  |
| perl-File-ShareDir |  |  |
| perl-File-Slurp |  |  |
| perl-Git-SVN |  |  |
| perl-GSSAPI |  |  |
| perl-hivex |  |  |
| perl-IO-Multiplex |  |  |
| perl-IO-Socket-INET6 |  |  |
| perl-IO-String |  |  |
| perl-JSON |  |  |
| perl-LDAP |  |  |
| perl-libintl-perl |  |  |
| perl-libxml-perl |  |  |
| perl-Mail-DKIM |  |  |
| perl-Mail-Sender |  |  |
| perl-Mail-SPF |  |  |
| perl-MailTools |  |  |
| perl-Module-Pluggable |  |  |
| perl-Module-Runtime |  |  |
| perl-Mozilla-LDAP |  |  |
| perl-NetAddr-IP |  |  |
| perl-Net-DNS |  |  |
| perl-Net-Server |  |  |
| perl-Net-SMTP-SSL |  |  |
| perl-PCP-LogImport |  |  |
| perl-PCP-LogSummary |  |  |
| perl-PCP-MMV |  |  |
| perl-PCP-PMDA |  |  |
| perl-Pod-LaTeX |  |  |
| perl-Pod-Plainer |  |  |
| perl-SNMP_Session |  |  |
| perl-Socket6 |  |  |
| perl-String-CRC32 |  |  |
| perl-Sys-Guestfs |  |  |
| perl-Sys-Virt |  |  |
| perl-Text-Soundex |  |  |
| perl-Text-Unidecode |  |  |
| perl-Tk |  |  |
| perl-Unix-Syslog |  |  |
| perl-XML-Catalog |  |  |
| perl-XML-LibXML |  |  |
| perl-XML-NamespaceSupport |  |  |
| perl-XML-Parser |  |  |
| perl-XML-SAX |  |  |
| perl-XML-SAX-Base |  |  |
| perl-XML-Simple |  |  |
| perl-XML-TokeParser |  |  |
| perl-XML-XPath |  |  |
| pesign |  |  |
| pgaudit |  |  |
| pg_repack |  |  |
| pidgin |  |  |
| pidgin-sipe |  |  |
| pinentry-emacs |  |  |
| pinentry-gnome3 |  |  |
| pinentry-gtk |  |  |
| pinfo |  |  |
| pipewire |  |  |
| pipewire0.2-devel |  |  |
| pipewire0.2-libs |  |  |
| pipewire-devel |  |  |
| pipewire-doc |  |  |
| pipewire-libs |  |  |
| pipewire-utils |  |  |
| pixman-devel |  |  |
| pki-acme |  |  |
| pki-base |  |  |
| pki-base-java |  |  |
| pki-ca |  |  |
| pki-kra |  |  |
| pki-server |  |  |
| pki-servlet-4.0-api |  |  |
| pki-servlet-container |  |  |
| pki-servlet-engine |  |  |
| pki-symkey |  |  |
| pki-tools |  |  |
| platform-python-coverage |  |  |
| plymouth |  |  |
| plymouth-core-libs |  |  |
| plymouth-graphics-libs |  |  |
| plymouth-plugin-fade-throbber |  |  |
| plymouth-plugin-label |  |  |
| plymouth-plugin-script |  |  |
| plymouth-plugin-space-flares |  |  |
| plymouth-plugin-throbgress |  |  |
| plymouth-plugin-two-step |  |  |
| plymouth-scripts |  |  |
| plymouth-system-theme |  |  |
| plymouth-theme-charge |  |  |
| plymouth-theme-fade-in |  |  |
| plymouth-theme-script |  |  |
| plymouth-theme-solar |  |  |
| plymouth-theme-spinfinity |  |  |
| plymouth-theme-spinner |  |  |
| pmdk-convert |  |  |
| pmempool |  |  |
| pmix |  |  |
| pmreorder |  |  |
| pnm2ppa |  |  |
| podman-manpages |  |  |
| policycoreutils-gui |  |  |
| policycoreutils-sandbox |  |  |
| poppler-glib |  |  |
| postfix-cdb |  |  |
| postfix-ldap |  |  |
| postfix-mysql |  |  |
| postfix-pcre |  |  |
| postfix-perl-scripts |  |  |
| postfix-pgsql |  |  |
| postfix-sqlite |  |  |
| postgres-decoderbufs |  |  |
| postgresql |  |  |
| postgresql-contrib |  |  |
| postgresql-docs |  |  |
| postgresql-jdbc |  |  |
| postgresql-jdbc-javadoc |  |  |
| postgresql-odbc |  |  |
| postgresql-odbc-tests |  |  |
| postgresql-plperl |  |  |
| postgresql-plpython3 |  |  |
| postgresql-pltcl |  |  |
| postgresql-server |  |  |
| postgresql-server-devel |  |  |
| postgresql-static |  |  |
| postgresql-test |  |  |
| postgresql-test-rpm-macros |  |  |
| postgresql-upgrade |  |  |
| postgresql-upgrade-devel |  |  |
| potrace |  |  |
| powertop |  |  |
| pptp |  |  |
| procmail |  |  |
| protobuf |  |  |
| protobuf-c-compiler |  |  |
| protobuf-c-devel |  |  |
| protobuf-compiler |  |  |
| protobuf-lite |  |  |
| pulseaudio |  |  |
| pulseaudio-libs-devel |  |  |
| pulseaudio-libs-glib2 |  |  |
| pulseaudio-module-bluetooth |  |  |
| pulseaudio-module-x11 |  |  |
| pulseaudio-utils |  |  |
| purple-sipe |  |  |
| pygobject2 |  |  |
| pygobject2-codegen |  |  |
| pygobject2-devel |  |  |
| pygobject2-doc |  |  |
| pygtk2 |  |  |
| pygtk2-codegen |  |  |
| pygtk2-devel |  |  |
| pygtk2-doc |  |  |
| pykickstart |  |  |
| python2-cairo |  |  |
| python2-cairo-devel |  |  |
| python2-scour |  |  |
| python3-abrt |  |  |
| python3-abrt-addon |  |  |
| python3-abrt-container-addon |  |  |
| python3-abrt-doc |  |  |
| python3-argcomplete |  |  |
| python3-argh |  |  |
| python3-augeas |  |  |
| python3-bcc |  |  |
| python3-blivet |  |  |
| python3-blockdev |  |  |
| python3-brlapi |  |  |
| python3-brotli |  |  |
| python3-bytesize |  |  |
| python3-cairo |  |  |
| python3-click |  |  |
| python3-coverage |  |  |
| python3-cpio |  |  |
| python3-createrepo_c |  |  |
| python3-cups |  |  |
| python3-custodia |  |  |
| python3-dasbus |  |  |
| python3-dbus-client-gen |  |  |
| python3-dbus-python-client-gen |  |  |
| python3-dbus-signature-pyparsing |  |  |
| python3-dnf-plugin-spacewalk |  |  |
| python3-enchant |  |  |
| python3-evdev |  |  |
| python3-flask |  |  |
| python3-freeradius |  |  |
| python3-gevent |  |  |
| python3-gobject |  |  |
| python3-greenlet |  |  |
| python3-gssapi |  |  |
| python3-hivex |  |  |
| python3-humanize |  |  |
| python3-hwdata |  |  |
| python3-into-dbus-python |  |  |
| python3-ipaclient |  |  |
| python3-ipalib |  |  |
| python3-ipaserver |  |  |
| python3-ipatests |  |  |
| python3-itsdangerous |  |  |
| python3-jabberpy |  |  |
| python3-jmespath |  |  |
| python3-jsonpatch |  |  |
| python3-jsonpointer |  |  |
| python3-jsonschema |  |  |
| python3-justbases |  |  |
| python3-justbytes |  |  |
| python3-jwcrypto |  |  |
| python3-kdcproxy |  |  |
| python3-keycloak-httpd-client-install |  |  |
| python3-kickstart |  |  |
| python3-koan |  |  |
| python3-langtable |  |  |
| python3-ldap |  |  |
| python3-lib389 |  |  |
| python3-libguestfs |  |  |
| python3-libmodulemd |  |  |
| python3-libmount |  |  |
| python3-libnbd |  |  |
| python3-libnmstate |  |  |
| python3-libreport |  |  |
| python3-libvirt |  |  |
| python3-libvoikko |  |  |
| python3-louis |  |  |
| python3-mako |  |  |
| python3-meh |  |  |
| python3-meh-gui |  |  |
| python3-netaddr |  |  |
| python3-netifaces |  |  |
| python3-networkx |  |  |
| python3-networkx-core |  |  |
| python3-newt |  |  |
| python3-nispor |  |  |
| python3-nss |  |  |
| python3-ntplib |  |  |
| python3-ordered-set |  |  |
| python3-osa-common |  |  |
| python3-osad |  |  |
| python3-osbuild |  |  |
| python3-pcp |  |  |
| python3-pexpect |  |  |
| python3-pid |  |  |
| python3-pillow |  |  |
| python3-pip-wheel |  |  |
| python3-pki |  |  |
| python3-prettytable |  |  |
| python3-productmd |  |  |
| python3-psutil |  |  |
| python3-ptyprocess |  |  |
| python3-pyasn1 |  |  |
| python3-pyasn1-modules |  |  |
| python3-pyatspi |  |  |
| python3-pydbus |  |  |
| python3-pyghmi |  |  |
| python3-pyodbc |  |  |
| python3-pyparted |  |  |
| python3-pyqt5-sip |  |  |
| python3-pyserial |  |  |
| python3-pyusb |  |  |
| python3-qrcode |  |  |
| python3-qrcode-core |  |  |
| python3-qt5 |  |  |
| python3-qt5-base |  |  |
| python3-reportlab |  |  |
| python3-requests-file |  |  |
| python3-requests-ftp |  |  |
| python3-rhncfg |  |  |
| python3-rhncfg-actions |  |  |
| python3-rhncfg-client |  |  |
| python3-rhncfg-management |  |  |
| python3-rhn-check |  |  |
| python3-rhn-client-tools |  |  |
| python3-rhnlib |  |  |
| python3-rhnpush |  |  |
| python3-rhn-setup |  |  |
| python3-rhn-setup-gnome |  |  |
| python3-rhn-virtualization-common |  |  |
| python3-rhn-virtualization-host |  |  |
| python3-rpmfluff |  |  |
| python3-sanlock |  |  |
| python3-semantic_version |  |  |
| python3-setuptools-wheel |  |  |
| python3-simpleline |  |  |
| python3-sip |  |  |
| python3-spacewalk-abrt |  |  |
| python3-spacewalk-backend-libs |  |  |
| python3-spacewalk-koan |  |  |
| python3-spacewalk-oscap |  |  |
| python3-spacewalk-usix |  |  |
| python3-speechd |  |  |
| python3-subversion |  |  |
| python3-suds |  |  |
| python3-sushy |  |  |
| python3-systemd |  |  |
| python3-tbb |  |  |
| python3-tracer |  |  |
| python3-werkzeug |  |  |
| python3-wx-siplib |  |  |
| python3-yubico |  |  |
| python-nss-doc |  |  |
| python-podman-api |  |  |
| python-qt5-rpm-macros |  |  |
| qatengine |  |  |
| qatlib |  |  |
| qemu-guest-agent |  |  |
| qemu-img |  |  |
| qemu-kvm |  |  |
| qemu-kvm-block-curl |  |  |
| qemu-kvm-block-gluster |  |  |
| qemu-kvm-block-iscsi |  |  |
| qemu-kvm-block-rbd |  |  |
| qemu-kvm-block-ssh |  |  |
| qemu-kvm-common |  |  |
| qemu-kvm-core |  |  |
| qgnomeplatform |  |  |
| qgpgme |  |  |
| qpdf |  |  |
| qpdf-doc |  |  |
| qperf |  |  |
| qrencode |  |  |
| qt5-assistant |  |  |
| qt5-designer |  |  |
| qt5-doctools |  |  |
| qt5-linguist |  |  |
| qt5-qdbusviewer |  |  |
| qt5-qt3d |  |  |
| qt5-qt3d-devel |  |  |
| qt5-qt3d-examples |  |  |
| qt5-qtbase |  |  |
| qt5-qtbase-common |  |  |
| qt5-qtbase-devel |  |  |
| qt5-qtbase-examples |  |  |
| qt5-qtbase-gui |  |  |
| qt5-qtbase-mysql |  |  |
| qt5-qtbase-odbc |  |  |
| qt5-qtbase-postgresql |  |  |
| qt5-qtbase-private-devel |  |  |
| qt5-qtcanvas3d |  |  |
| qt5-qtcanvas3d-examples |  |  |
| qt5-qtconnectivity |  |  |
| qt5-qtconnectivity-devel |  |  |
| qt5-qtconnectivity-examples |  |  |
| qt5-qtdeclarative |  |  |
| qt5-qtdeclarative-devel |  |  |
| qt5-qtdeclarative-examples |  |  |
| qt5-qtdoc |  |  |
| qt5-qtgraphicaleffects |  |  |
| qt5-qtimageformats |  |  |
| qt5-qtlocation |  |  |
| qt5-qtlocation-devel |  |  |
| qt5-qtlocation-examples |  |  |
| qt5-qtmultimedia |  |  |
| qt5-qtmultimedia-devel |  |  |
| qt5-qtmultimedia-examples |  |  |
| qt5-qtquickcontrols |  |  |
| qt5-qtquickcontrols2 |  |  |
| qt5-qtquickcontrols2-examples |  |  |
| qt5-qtquickcontrols-examples |  |  |
| qt5-qtscript |  |  |
| qt5-qtscript-devel |  |  |
| qt5-qtscript-examples |  |  |
| qt5-qtsensors |  |  |
| qt5-qtsensors-devel |  |  |
| qt5-qtsensors-examples |  |  |
| qt5-qtserialbus |  |  |
| qt5-qtserialbus-examples |  |  |
| qt5-qtserialport |  |  |
| qt5-qtserialport-devel |  |  |
| qt5-qtserialport-examples |  |  |
| qt5-qtsvg |  |  |
| qt5-qtsvg-devel |  |  |
| qt5-qtsvg-examples |  |  |
| qt5-qttools |  |  |
| qt5-qttools-common |  |  |
| qt5-qttools-devel |  |  |
| qt5-qttools-examples |  |  |
| qt5-qttools-libs-designer |  |  |
| qt5-qttools-libs-designercomponents |  |  |
| qt5-qttools-libs-help |  |  |
| qt5-qttranslations |  |  |
| qt5-qtwayland |  |  |
| qt5-qtwayland-examples |  |  |
| qt5-qtwebchannel |  |  |
| qt5-qtwebchannel-devel |  |  |
| qt5-qtwebchannel-examples |  |  |
| qt5-qtwebsockets |  |  |
| qt5-qtwebsockets-devel |  |  |
| qt5-qtwebsockets-examples |  |  |
| qt5-qtx11extras |  |  |
| qt5-qtx11extras-devel |  |  |
| qt5-qtxmlpatterns |  |  |
| qt5-qtxmlpatterns-devel |  |  |
| qt5-qtxmlpatterns-examples |  |  |
| qt5-rpm-macros |  |  |
| radvd |  |  |
| raptor2 |  |  |
| rarian |  |  |
| rarian-compat |  |  |
| rasqal |  |  |
| rear |  |  |
| redfish-finder |  |  |
| redhat-backgrounds |  |  |
| redhat-logos-ipa |  |  |
| redhat-lsb |  |  |
| redhat-lsb-cxx |  |  |
| redhat-lsb-desktop |  |  |
| redhat-lsb-languages |  |  |
| redhat-lsb-printing |  |  |
| redhat-lsb-submod-multimedia |  |  |
| redhat-menus |  |  |
| redhat-support-lib-python |  |  |
| redhat-support-tool |  |  |
| redis |  |  |
| redis-devel |  |  |
| redis-doc |  |  |
| redland |  |  |
| relaxngcc-javadoc |  |  |
| relaxngDatatype |  |  |
| relaxngDatatype-javadoc |  |  |
| resteasy |  |  |
| resteasy-javadoc |  |  |
| rhc |  |  |
| rhc-worker-playbook |  |  |
| rhel-system-roles |  |  |
| rhncfg |  |  |
| rhncfg-actions |  |  |
| rhncfg-client |  |  |
| rhncfg-management |  |  |
| rhn-check |  |  |
| rhn-client-tools |  |  |
| rhn-custom-info |  |  |
| rhnlib |  |  |
| rhnpush |  |  |
| rhnsd |  |  |
| rhn-setup |  |  |
| rhn-setup-gnome |  |  |
| rhn-virtualization-host |  |  |
| rhsm-gtk |  |  |
| rhythmbox |  |  |
| rpmdevtools |  |  |
| rpmemd |  |  |
| rpmlint |  |  |
| rpm-mpi-hooks |  |  |
| rpm-ostree |  |  |
| rpm-ostree-libs |  |  |
| rpm-plugin-fapolicyd |  |  |
| rrdtool-perl |  |  |
| rshim |  |  |
| rsyslog-crypto |  |  |
| rsyslog-doc |  |  |
| rsyslog-elasticsearch |  |  |
| rsyslog-gnutls |  |  |
| rsyslog-gssapi |  |  |
| rsyslog-kafka |  |  |
| rsyslog-mmaudit |  |  |
| rsyslog-mmjsonparse |  |  |
| rsyslog-mmkubernetes |  |  |
| rsyslog-mmnormalize |  |  |
| rsyslog-mmsnmptrapd |  |  |
| rsyslog-mysql |  |  |
| rsyslog-omamqp1 |  |  |
| rsyslog-openssl |  |  |
| rsyslog-pgsql |  |  |
| rsyslog-relp |  |  |
| rsyslog-snmp |  |  |
| rsyslog-udpspoof |  |  |
| rtkit |  |  |
| rt-tests |  |  |
| rubygem-bundler-doc |  |  |
| ruby-hivex |  |  |
| ruby-libguestfs |  |  |
| rust-lldb |  |  |
| saab-fonts |  |  |
| sac |  |  |
| samba-test |  |  |
| samba-vfs-iouring |  |  |
| samyak-devanagari-fonts |  |  |
| samyak-fonts-common |  |  |
| samyak-gujarati-fonts |  |  |
| samyak-malayalam-fonts |  |  |
| samyak-odia-fonts |  |  |
| samyak-tamil-fonts |  |  |
| sane-backends |  |  |
| sane-backends-daemon |  |  |
| sane-backends-devel |  |  |
| sane-backends-doc |  |  |
| sane-backends-drivers-cameras |  |  |
| sane-backends-drivers-scanners |  |  |
| sane-backends-libs |  |  |
| sane-frontends |  |  |
| sanlk-reset |  |  |
| sanlock |  |  |
| sassist |  |  |
| sat4j |  |  |
| satyr |  |  |
| sbc |  |  |
| sbd |  |  |
| sblim-cmpi-base |  |  |
| sblim-gather |  |  |
| sblim-gather-provider |  |  |
| sblim-indication_helper |  |  |
| sblim-sfcb |  |  |
| sblim-sfcc |  |  |
| sblim-sfcCommon |  |  |
| sblim-wbemcli |  |  |
| scala |  |  |
| scala-apidoc |  |  |
| scala-swing |  |  |
| scap-security-guide |  |  |
| scap-security-guide-doc |  |  |
| scap-workbench |  |  |
| scrub |  |  |
| SDL |  |  |
| SDL-devel |  |  |
| seabios |  |  |
| seabios-bin |  |  |
| seahorse |  |  |
| seavgabios-bin |  |  |
| sendmail |  |  |
| sendmail-cf |  |  |
| sendmail-doc |  |  |
| sendmail-milter |  |  |
| setools |  |  |
| setools-console-analyses |  |  |
| setools-gui |  |  |
| setroubleshoot |  |  |
| setroubleshoot-plugins |  |  |
| setroubleshoot-server |  |  |
| sevctl |  |  |
| sgabios |  |  |
| sgabios-bin |  |  |
| sil-abyssinica-fonts |  |  |
| sil-nuosu-fonts |  |  |
| sil-padauk-book-fonts |  |  |
| sil-padauk-fonts |  |  |
| sil-scheherazade-fonts |  |  |
| sip |  |  |
| si-units |  |  |
| si-units-javadoc |  |  |
| skkdic |  |  |
| slang-devel |  |  |
| slapi-nis |  |  |
| slf4j-jdk14 |  |  |
| smc-anjalioldlipi-fonts |  |  |
| smc-dyuthi-fonts |  |  |
| smc-fonts-common |  |  |
| smc-kalyani-fonts |  |  |
| smc-meera-fonts |  |  |
| smc-rachana-fonts |  |  |
| smc-raghumalayalam-fonts |  |  |
| smc-suruma-fonts |  |  |
| softhsm |  |  |
| softhsm-devel |  |  |
| sos-collector |  |  |
| sound-theme-freedesktop |  |  |
| soundtouch |  |  |
| source-highlight |  |  |
| spacewalk-abrt |  |  |
| spacewalk-client-cert |  |  |
| spacewalk-koan |  |  |
| spacewalk-oscap |  |  |
| spacewalk-remote-utils |  |  |
| spacewalk-usix |  |  |
| spamassassin |  |  |
| speech-dispatcher |  |  |
| speech-dispatcher-espeak-ng |  |  |
| speex |  |  |
| speexdsp |  |  |
| spice-client-win-x64 |  |  |
| spice-client-win-x86 |  |  |
| spice-glib |  |  |
| spice-glib-devel |  |  |
| spice-gtk |  |  |
| spice-gtk3 |  |  |
| spice-gtk3-devel |  |  |
| spice-gtk3-vala |  |  |
| spice-gtk-tools |  |  |
| spice-protocol |  |  |
| spice-qxl-wddm-dod |  |  |
| spice-qxl-xddm |  |  |
| spice-server |  |  |
| spice-streaming-agent |  |  |
| spice-vdagent |  |  |
| spice-vdagent-win-x64 |  |  |
| spice-vdagent-win-x86 |  |  |
| spirv-tools |  |  |
| spirv-tools-libs |  |  |
| splix |  |  |
| squid |  |  |
| stalld |  |  |
| startup-notification |  |  |
| startup-notification-devel |  |  |
| stax-ex |  |  |
| stax-ex-javadoc |  |  |
| stix-fonts |  |  |
| stix-math-fonts |  |  |
| stratis-cli |  |  |
| stratisd |  |  |
| stratisd-dracut |  |  |
| subscription-manager-initial-setup-addon |  |  |
| subscription-manager-migration |  |  |
| subscription-manager-migration-data |  |  |
| subversion |  |  |
| subversion-devel |  |  |
| subversion-gnome |  |  |
| subversion-javahl |  |  |
| subversion-libs |  |  |
| subversion-perl |  |  |
| subversion-tools |  |  |
| suitesparse |  |  |
| supermin |  |  |
| supermin-devel |  |  |
| sushi |  |  |
| swig |  |  |
| swig-doc |  |  |
| swig-gdb |  |  |
| switcheroo-control |  |  |
| sysfsutils |  |  |
| sysstat |  |  |
| system-config-printer-libs |  |  |
| system-config-printer-udev |  |  |
| systemtap |  |  |
| systemtap-client |  |  |
| systemtap-devel |  |  |
| systemtap-exporter |  |  |
| systemtap-initscript |  |  |
| systemtap-runtime |  |  |
| systemtap-runtime-java |  |  |
| systemtap-runtime-python3 |  |  |
| systemtap-runtime-virtguest |  |  |
| systemtap-runtime-virthost |  |  |
| systemtap-server |  |  |
| taglib |  |  |
| tagsoup |  |  |
| tang |  |  |
| targetcli |  |  |
| tbb |  |  |
| tbb-devel |  |  |
| tbb-doc |  |  |
| tcl-brlapi |  |  |
| tcpdump |  |  |
| tcsh |  |  |
| teckit |  |  |
| telnet |  |  |
| telnet-server |  |  |
| tesseract |  |  |
| tex-fonts-hebrew |  |  |
| texlive |  |  |
| texlive-adjustbox |  |  |
| texlive-ae |  |  |
| texlive-algorithms |  |  |
| texlive-amscls |  |  |
| texlive-amsfonts |  |  |
| texlive-amsmath |  |  |
| texlive-anyfontsize |  |  |
| texlive-anysize |  |  |
| texlive-appendix |  |  |
| texlive-arabxetex |  |  |
| texlive-arphic |  |  |
| texlive-attachfile |  |  |
| texlive-avantgar |  |  |
| texlive-awesomebox |  |  |
| texlive-babel |  |  |
| texlive-babelbib |  |  |
| texlive-babel-english |  |  |
| texlive-base |  |  |
| texlive-beamer |  |  |
| texlive-bera |  |  |
| texlive-beton |  |  |
| texlive-bibtex |  |  |
| texlive-bibtopic |  |  |
| texlive-bidi |  |  |
| texlive-bigfoot |  |  |
| texlive-bookman |  |  |
| texlive-booktabs |  |  |
| texlive-breakurl |  |  |
| texlive-breqn |  |  |
| texlive-caption |  |  |
| texlive-capt-of |  |  |
| texlive-carlisle |  |  |
| texlive-changebar |  |  |
| texlive-changepage |  |  |
| texlive-charter |  |  |
| texlive-chngcntr |  |  |
| texlive-cite |  |  |
| texlive-cjk |  |  |
| texlive-classpack |  |  |
| texlive-cm |  |  |
| texlive-cmap |  |  |
| texlive-cmextra |  |  |
| texlive-cm-lgc |  |  |
| texlive-cm-super |  |  |
| texlive-cns |  |  |
| texlive-collectbox |  |  |
| texlive-collection-basic |  |  |
| texlive-collection-fontsrecommended |  |  |
| texlive-collection-htmlxml |  |  |
| texlive-collection-latex |  |  |
| texlive-collection-latexrecommended |  |  |
| texlive-collection-xetex |  |  |
| texlive-colortbl |  |  |
| texlive-context |  |  |
| texlive-courier |  |  |
| texlive-crop |  |  |
| texlive-csquotes |  |  |
| texlive-ctable |  |  |
| texlive-ctablestack |  |  |
| texlive-currfile |  |  |
| texlive-datetime |  |  |
| texlive-dvipdfmx |  |  |
| texlive-dvipng |  |  |
| texlive-dvips |  |  |
| texlive-dvisvgm |  |  |
| texlive-ec |  |  |
| texlive-eepic |  |  |
| texlive-enctex |  |  |
| texlive-enumitem |  |  |
| texlive-environ |  |  |
| texlive-epsf |  |  |
| texlive-epstopdf |  |  |
| texlive-eqparbox |  |  |
| texlive-eso-pic |  |  |
| texlive-etex |  |  |
| texlive-etex-pkg |  |  |
| texlive-etoolbox |  |  |
| texlive-euenc |  |  |
| texlive-euler |  |  |
| texlive-euro |  |  |
| texlive-eurosym |  |  |
| texlive-extsizes |  |  |
| texlive-fancybox |  |  |
| texlive-fancyhdr |  |  |
| texlive-fancyref |  |  |
| texlive-fancyvrb |  |  |
| texlive-filecontents |  |  |
| texlive-filehook |  |  |
| texlive-finstrut |  |  |
| texlive-fix2col |  |  |
| texlive-fixlatvian |  |  |
| texlive-float |  |  |
| texlive-fmtcount |  |  |
| texlive-fncychap |  |  |
| texlive-fontawesome |  |  |
| texlive-fontbook |  |  |
| texlive-fontspec |  |  |
| texlive-fonts-tlwg |  |  |
| texlive-fontware |  |  |
| texlive-fontwrap |  |  |
| texlive-footmisc |  |  |
| texlive-fp |  |  |
| texlive-fpl |  |  |
| texlive-framed |  |  |
| texlive-garuda-c90 |  |  |
| texlive-geometry |  |  |
| texlive-glyphlist |  |  |
| texlive-graphics |  |  |
| texlive-graphics-cfg |  |  |
| texlive-graphics-def |  |  |
| texlive-gsftopk |  |  |
| texlive-helvetic |  |  |
| texlive-hyperref |  |  |
| texlive-hyphenat |  |  |
| texlive-hyphen-base |  |  |
| texlive-hyph-utf8 |  |  |
| texlive-ifetex |  |  |
| texlive-ifluatex |  |  |
| texlive-ifmtarg |  |  |
| texlive-ifoddpage |  |  |
| texlive-iftex |  |  |
| texlive-ifxetex |  |  |
| texlive-import |  |  |
| texlive-index |  |  |
| texlive-jadetex |  |  |
| texlive-jknapltx |  |  |
| texlive-kastrup |  |  |
| texlive-kerkis |  |  |
| texlive-knuth-lib |  |  |
| texlive-knuth-local |  |  |
| texlive-koma-script |  |  |
| texlive-kpathsea |  |  |
| texlive-l3experimental |  |  |
| texlive-l3kernel |  |  |
| texlive-l3packages |  |  |
| texlive-lastpage |  |  |
| texlive-latex |  |  |
| texlive-latex2man |  |  |
| texlive-latexconfig |  |  |
| texlive-latex-fonts |  |  |
| texlive-lettrine |  |  |
| texlive-lib |  |  |
| texlive-linegoal |  |  |
| texlive-lineno |  |  |
| texlive-listings |  |  |
| texlive-lm |  |  |
| texlive-lm-math |  |  |
| texlive-ltabptch |  |  |
| texlive-ltxmisc |  |  |
| texlive-lua-alt-getopt |  |  |
| texlive-lualatex-math |  |  |
| texlive-lualibs |  |  |
| texlive-luaotfload |  |  |
| texlive-luatex |  |  |
| texlive-luatex85 |  |  |
| texlive-luatexbase |  |  |
| texlive-makecmds |  |  |
| texlive-makeindex |  |  |
| texlive-manfnt-font |  |  |
| texlive-marginnote |  |  |
| texlive-marvosym |  |  |
| texlive-mathpazo |  |  |
| texlive-mathspec |  |  |
| texlive-mathtools |  |  |
| texlive-mdwtools |  |  |
| texlive-memoir |  |  |
| texlive-metafont |  |  |
| texlive-metalogo |  |  |
| texlive-metapost |  |  |
| texlive-mflogo |  |  |
| texlive-mflogo-font |  |  |
| texlive-mfnfss |  |  |
| texlive-mfware |  |  |
| texlive-microtype |  |  |
| texlive-mnsymbol |  |  |
| texlive-mparhack |  |  |
| texlive-mptopdf |  |  |
| texlive-ms |  |  |
| texlive-multido |  |  |
| texlive-multirow |  |  |
| texlive-natbib |  |  |
| texlive-ncctools |  |  |
| texlive-ncntrsbk |  |  |
| texlive-needspace |  |  |
| texlive-norasi-c90 |  |  |
| texlive-ntgclass |  |  |
| texlive-oberdiek |  |  |
| texlive-overpic |  |  |
| texlive-palatino |  |  |
| texlive-paralist |  |  |
| texlive-parallel |  |  |
| texlive-parskip |  |  |
| texlive-passivetex |  |  |
| texlive-pdfpages |  |  |
| texlive-pdftex |  |  |
| texlive-pgf |  |  |
| texlive-philokalia |  |  |
| texlive-placeins |  |  |
| texlive-plain |  |  |
| texlive-polyglossia |  |  |
| texlive-powerdot |  |  |
| texlive-preprint |  |  |
| texlive-psfrag |  |  |
| texlive-pslatex |  |  |
| texlive-psnfss |  |  |
| texlive-pspicture |  |  |
| texlive-pst-3d |  |  |
| texlive-pst-arrow |  |  |
| texlive-pst-blur |  |  |
| texlive-pst-coil |  |  |
| texlive-pst-eps |  |  |
| texlive-pst-fill |  |  |
| texlive-pst-grad |  |  |
| texlive-pst-math |  |  |
| texlive-pst-node |  |  |
| texlive-pst-plot |  |  |
| texlive-pstricks |  |  |
| texlive-pstricks-add |  |  |
| texlive-pst-slpe |  |  |
| texlive-pst-text |  |  |
| texlive-pst-tools |  |  |
| texlive-pst-tree |  |  |
| texlive-ptext |  |  |
| texlive-pxfonts |  |  |
| texlive-qstest |  |  |
| texlive-rcs |  |  |
| texlive-realscripts |  |  |
| texlive-rsfs |  |  |
| texlive-sansmath |  |  |
| texlive-sauerj |  |  |
| texlive-scheme-basic |  |  |
| texlive-section |  |  |
| texlive-sectsty |  |  |
| texlive-seminar |  |  |
| texlive-sepnum |  |  |
| texlive-setspace |  |  |
| texlive-showexpl |  |  |
| texlive-soul |  |  |
| texlive-stmaryrd |  |  |
| texlive-subfig |  |  |
| texlive-subfigure |  |  |
| texlive-svn-prov |  |  |
| texlive-symbol |  |  |
| texlive-t2 |  |  |
| texlive-tabu |  |  |
| texlive-tabulary |  |  |
| texlive-tetex |  |  |
| texlive-tex |  |  |
| texlive-tex4ht |  |  |
| texlive-texconfig |  |  |
| texlive-tex-gyre |  |  |
| texlive-tex-gyre-math |  |  |
| texlive-tex-ini-files |  |  |
| texlive-texlive-common-doc |  |  |
| texlive-texlive-docindex |  |  |
| texlive-texlive-en |  |  |
| texlive-texlive.infra |  |  |
| texlive-texlive-msg-translations |  |  |
| texlive-texlive-scripts |  |  |
| texlive-textcase |  |  |
| texlive-textpos |  |  |
| texlive-threeparttable |  |  |
| texlive-thumbpdf |  |  |
| texlive-times |  |  |
| texlive-tipa |  |  |
| texlive-titlesec |  |  |
| texlive-titling |  |  |
| texlive-tocloft |  |  |
| texlive-tools |  |  |
| texlive-trimspaces |  |  |
| texlive-txfonts |  |  |
| texlive-type1cm |  |  |
| texlive-typehtml |  |  |
| texlive-ucharclasses |  |  |
| texlive-ucs |  |  |
| texlive-uhc |  |  |
| texlive-ulem |  |  |
| texlive-underscore |  |  |
| texlive-unicode-data |  |  |
| texlive-unicode-math |  |  |
| texlive-unisugar |  |  |
| texlive-updmap-map |  |  |
| texlive-upquote |  |  |
| texlive-url |  |  |
| texlive-utopia |  |  |
| texlive-varwidth |  |  |
| texlive-wadalab |  |  |
| texlive-was |  |  |
| texlive-wasy |  |  |
| texlive-wasy2-ps |  |  |
| texlive-wasysym |  |  |
| texlive-wrapfig |  |  |
| texlive-xcolor |  |  |
| texlive-xdvi |  |  |
| texlive-xecjk |  |  |
| texlive-xecolor |  |  |
| texlive-xecyr |  |  |
| texlive-xeindex |  |  |
| texlive-xepersian |  |  |
| texlive-xesearch |  |  |
| texlive-xetex |  |  |
| texlive-xetexconfig |  |  |
| texlive-xetexfontinfo |  |  |
| texlive-xetex-itrans |  |  |
| texlive-xetex-pstricks |  |  |
| texlive-xetex-tibetan |  |  |
| texlive-xifthen |  |  |
| texlive-xkeyval |  |  |
| texlive-xltxtra |  |  |
| texlive-xmltex |  |  |
| texlive-xmltexconfig |  |  |
| texlive-xstring |  |  |
| texlive-xtab |  |  |
| texlive-xunicode |  |  |
| texlive-zapfchan |  |  |
| texlive-zapfding |  |  |
| tftp |  |  |
| tftp-server |  |  |
| thai-scalable-fonts-common |  |  |
| thai-scalable-garuda-fonts |  |  |
| thai-scalable-kinnari-fonts |  |  |
| thai-scalable-laksaman-fonts |  |  |
| thai-scalable-loma-fonts |  |  |
| thai-scalable-norasi-fonts |  |  |
| thai-scalable-purisa-fonts |  |  |
| thai-scalable-sawasdee-fonts |  |  |
| thai-scalable-tlwgmono-fonts |  |  |
| thai-scalable-tlwgtypewriter-fonts |  |  |
| thai-scalable-tlwgtypist-fonts |  |  |
| thai-scalable-tlwgtypo-fonts |  |  |
| thai-scalable-umpush-fonts |  |  |
| thai-scalable-waree-fonts |  |  |
| theora-tools |  |  |
| thermald |  |  |
| thunderbird |  |  |
| tibetan-machine-uni-fonts |  |  |
| tigervnc |  |  |
| tigervnc-icons |  |  |
| tigervnc-license |  |  |
| tigervnc-selinux |  |  |
| tigervnc-server |  |  |
| tigervnc-server-applet |  |  |
| tigervnc-server-minimal |  |  |
| tigervnc-server-module |  |  |
| tinycdb |  |  |
| tk-devel |  |  |
| tlog |  |  |
| tog-pegasus |  |  |
| tog-pegasus-libs |  |  |
| tomcatjss |  |  |
| toolbox |  |  |
| torque-libs |  |  |
| totem |  |  |
| totem-nautilus |  |  |
| totem-pl-parser |  |  |
| tracer-common |  |  |
| tracker |  |  |
| tracker-miners |  |  |
| tuned-gtk |  |  |
| tuned-utils |  |  |
| tuned-utils-systemtap |  |  |
| turbojpeg |  |  |
| twolame-libs |  |  |
| ucs-miscfixed-fonts |  |  |
| ucx |  |  |
| ucx-cma |  |  |
| ucx-devel |  |  |
| ucx-ib |  |  |
| ucx-rdmacm |  |  |
| udftools |  |  |
| udisks2 |  |  |
| udisks2-iscsi |  |  |
| udisks2-lsm |  |  |
| udisks2-lvm2 |  |  |
| unbound |  |  |
| unicode-ucd |  |  |
| unit-api |  |  |
| unit-api-javadoc |  |  |
| univocity-parsers |  |  |
| uom-lib |  |  |
| uom-lib-javadoc |  |  |
| uom-parent |  |  |
| uom-se |  |  |
| uom-se-javadoc |  |  |
| uom-systems |  |  |
| uom-systems-javadoc |  |  |
| upower |  |  |
| urlview |  |  |
| usbguard |  |  |
| usbguard-dbus |  |  |
| usbguard-notifier |  |  |
| usbguard-selinux |  |  |
| usbguard-tools |  |  |
| usbmuxd |  |  |
| usbredir |  |  |
| usbredir-devel |  |  |
| usermode-gtk |  |  |
| utf8proc |  |  |
| valgrind |  |  |
| valgrind-devel |  |  |
| varnish |  |  |
| varnish-devel |  |  |
| varnish-docs |  |  |
| varnish-modules |  |  |
| velocity |  |  |
| velocity-demo |  |  |
| velocity-javadoc |  |  |
| velocity-manual |  |  |
| vhostmd |  |  |
| vim-X11 |  |  |
| vinagre |  |  |
| vino |  |  |
| virt-dib |  |  |
| virt-install |  |  |
| virtio-win |  |  |
| virt-manager |  |  |
| virt-manager-common |  |  |
| virt-p2v-maker |  |  |
| virt-top |  |  |
| virt-v2v |  |  |
| virt-viewer |  |  |
| virt-who |  |  |
| voikko-tools |  |  |
| volume_key |  |  |
| volume_key-devel |  |  |
| volume_key-libs |  |  |
| vorbis-tools |  |  |
| vsftpd |  |  |
| vte291 |  |  |
| vulkan-headers |  |  |
| vulkan-loader |  |  |
| vulkan-loader-devel |  |  |
| vulkan-tools |  |  |
| vulkan-validation-layers |  |  |
| WALinuxAgent |  |  |
| WALinuxAgent-udev |  |  |
| wavpack |  |  |
| wayland-devel |  |  |
| wayland-protocols-devel |  |  |
| webkit2gtk3 |  |  |
| webkit2gtk3-devel |  |  |
| webkit2gtk3-jsc |  |  |
| webkit2gtk3-jsc-devel |  |  |
| webkit2gtk3-plugin-process-gtk2 |  |  |
| webrtc-audio-processing |  |  |
| whois |  |  |
| whois-nls |  |  |
| wireshark |  |  |
| wireshark-cli |  |  |
| wodim |  |  |
| woff2 |  |  |
| wqy-microhei-fonts |  |  |
| wqy-unibit-fonts |  |  |
| wsmancli |  |  |
| x3270-x11 |  |  |
| xalan-j2 |  |  |
| xapian-core |  |  |
| xapian-core-libs |  |  |
| Xaw3d |  |  |
| xcb-util |  |  |
| xcb-util-image |  |  |
| xcb-util-keysyms |  |  |
| xcb-util-renderutil |  |  |
| xcb-util-wm |  |  |
| xdg-desktop-portal |  |  |
| xdg-desktop-portal-gtk |  |  |
| xdg-user-dirs |  |  |
| xdg-user-dirs-gtk |  |  |
| xdg-utils |  |  |
| xdp-tools |  |  |
| xerces-j2 |  |  |
| xerces-j2-demo |  |  |
| xerces-j2-javadoc |  |  |
| xinetd |  |  |
| xkeyboard-config-devel |  |  |
| xml-commons-apis |  |  |
| xml-commons-apis-javadoc |  |  |
| xml-commons-apis-manual |  |  |
| xml-commons-resolver |  |  |
| xml-commons-resolver-javadoc |  |  |
| xmlgraphics-commons |  |  |
| xmlsec1-nss |  |  |
| xmlstreambuffer |  |  |
| xmlstreambuffer-javadoc |  |  |
| xml-stylebook |  |  |
| xml-stylebook-demo |  |  |
| xml-stylebook-javadoc |  |  |
| xmlto |  |  |
| xorg-sgml-doctools |  |  |
| xorg-x11-docs |  |  |
| xorg-x11-drivers |  |  |
| xorg-x11-drv-ati |  |  |
| xorg-x11-drv-dummy |  |  |
| xorg-x11-drv-evdev |  |  |
| xorg-x11-drv-evdev-devel |  |  |
| xorg-x11-drv-fbdev |  |  |
| xorg-x11-drv-intel |  |  |
| xorg-x11-drv-libinput |  |  |
| xorg-x11-drv-nouveau |  |  |
| xorg-x11-drv-qxl |  |  |
| xorg-x11-drv-v4l |  |  |
| xorg-x11-drv-vesa |  |  |
| xorg-x11-drv-vmware |  |  |
| xorg-x11-drv-wacom |  |  |
| xorg-x11-drv-wacom-serial-support |  |  |
| xorg-x11-fonts-100dpi |  |  |
| xorg-x11-fonts-75dpi |  |  |
| xorg-x11-fonts-cyrillic |  |  |
| xorg-x11-fonts-ethiopic |  |  |
| xorg-x11-fonts-ISO8859-1-100dpi |  |  |
| xorg-x11-fonts-ISO8859-14-100dpi |  |  |
| xorg-x11-fonts-ISO8859-14-75dpi |  |  |
| xorg-x11-fonts-ISO8859-15-100dpi |  |  |
| xorg-x11-fonts-ISO8859-15-75dpi |  |  |
| xorg-x11-fonts-ISO8859-1-75dpi |  |  |
| xorg-x11-fonts-ISO8859-2-100dpi |  |  |
| xorg-x11-fonts-ISO8859-2-75dpi |  |  |
| xorg-x11-fonts-ISO8859-9-100dpi |  |  |
| xorg-x11-fonts-ISO8859-9-75dpi |  |  |
| xorg-x11-fonts-misc |  |  |
| xorg-x11-server-common |  |  |
| xorg-x11-server-Xdmx |  |  |
| xorg-x11-server-Xephyr |  |  |
| xorg-x11-server-Xnest |  |  |
| xorg-x11-server-Xorg |  |  |
| xorg-x11-server-Xspice |  |  |
| xorg-x11-server-Xvfb |  |  |
| xorg-x11-server-Xwayland |  |  |
| xorg-x11-utils |  |  |
| xorg-x11-xbitmaps |  |  |
| xorg-x11-xinit |  |  |
| xorg-x11-xinit-session |  |  |
| xorg-x11-xkb-utils |  |  |
| xorriso |  |  |
| xrestop |  |  |
| xsane |  |  |
| xsane-common |  |  |
| xsane-gimp |  |  |
| xsom |  |  |
| xsom-javadoc |  |  |
| xterm |  |  |
| xterm-resize |  |  |
| xz-java |  |  |
| yelp |  |  |
| yelp-libs |  |  |
| yelp-tools |  |  |
| yelp-xsl |  |  |
| ypbind |  |  |
| ypserv |  |  |
| yp-tools |  |  |
| zenity |  |  |
| zsh-html |  |  |
| zziplib |  |  |
| zziplib-utils |  |  |
